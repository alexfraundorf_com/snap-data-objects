<?php
ini_set('display_errors', 1);

// require the composer autoloader
require_once('vendor/autoload.php');

// reference the factory class to save typing
use Snap\Data\Factory;


// create an instance of \Snap\Data\Address (description and notes are optional)
$Address = Factory::addressFactory('123 Fake Street', 'Appleton', 'WI', '54915', 
    'home', 'this is a note');
// update 
$Address->setAddress1('555 Fake Street');
// add additional information
$Address->setAddress2('Apt. 3B');
// get information
echo $Address->getAddress1() . '<br>'; // "555 Fake Street"
echo $Address->getAddress2() . '<br>'; // "Apt. 3B"
// validate (available statically)
$Address->validateZipCode('90210') . '<br>'; // true


// create an instance of \Snap\Data\CreditCard (some of these fields are optional)
// card number and expiration dates will be validated
$CreditCard = Factory::creditCardFactory('4250910000609650', '05', '44', '123', 
'541915', 'work card', 'this is a note');
// update
$CreditCard->setVerificationCode('987');
// get information
echo $CreditCard->getNumber() . '<br>'; // "4250910000609650"
echo $CreditCard->getVerificationCode() . '<br>'; // "123"
// validate (available statically)
$CreditCard->validateCardNumber('4250910000609650'); // true


// create an instance of \Snap\Data\Email (description and notes are optional)
// email address will be validated
$Email = Factory::emailFactory('john@email.com', 'work email', 'this is a note');
// update
$Email->setDescription('home email');
// get information
echo $Email->getEmail() . '<br>'; // "john@email.com"
echo $Email->getDescription() . '<br>'; // "home email"
// to string method
echo (string) $Email; // "john@email.com"
// validate (available statically)
$Email->validateEmail('jim@smith.com') . '<br>'; // true


// create an instance of \Snap\Data\Phone (description and notes are optional)
// phone number will be formatted and by default validated as a U.S. number
$Phone = Factory::phoneFactory('555-123-4567', 'work', 'this is a note');
$Phone2 = Factory::phoneFactory('5559876543');
// don't validate or format as a U.S. number
$InternationalPhone = Factory::phoneFactory('0300 200 3300', '', '', false);
// update
$Phone->setPhoneNumber('5551239876');
// get information
echo $Phone->getPhoneNumber() . '<br>'; // "(555) 123-9876"
echo $Phone2->getPhoneNumber() . '<br>'; // "(555) 987-6543"
echo $InternationalPhone->getPhoneNumber() . '<br>'; // "0300 200 3300"
// validate (available statically)
$Phone->validateUsPhoneNumber('5551234567') . '<br>'; // true


// create an instance of \Snap\Data\Person
$Person = Factory::personFactory();
// set some data
$Person
    ->setId('12345')
    ->setFirstName('Alex')
    ->setLastName('Fraundorf')
    ->buildFullName();
    ;
// get some data
echo $Person->getFirstName() . '<br>'; // "Alex"
echo $Person->getLastName() . '<br>'; // "Fraundorf"
echo $Person->getFullName() . '<br>'; // "Alex Fraundorf"

// assign some other data objects to the person (supports multiple)
$Person
    ->addAddressObject($Address)
    ->addCreditCardObject($CreditCard)
    ->addEmailAddressObject($Email)
    ->addPhoneNumberObject($Phone)
    ->addPhoneNumberObject($Phone2)
    ;

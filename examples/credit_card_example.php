<?php
ini_set('display_errors', 1);

// require the composer autoloader
require_once('vendor/autoload.php');

// reference the factory class to save typing
use Snap\Data\Factory;


// create an instance of \Snap\Data\CreditCard (some of these fields are optional)
// card number and expiration dates will be validated
$CreditCard = Factory::creditCardFactory('4250910000609650', '05', '44', '123', 
'541915', 'work card', 'this is a note');
// update
$CreditCard->setVerificationCode('987');
// get information
echo $CreditCard->getNumber() . '<br>'; // "4250910000609650"
echo $CreditCard->getVerificationCode() . '<br>'; // "123"
// validate (available statically)
$CreditCard->validateCardNumber('4250910000609650'); // true

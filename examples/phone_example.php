<?php
ini_set('display_errors', 1);

// require the composer autoloader
require_once('vendor/autoload.php');

// reference the factory class to save typing
use Snap\Data\Factory;


// create an instance of \Snap\Data\Phone (description and notes are optional)
// phone number will be formatted and by default validated as a U.S. number
$Phone = Factory::phoneFactory('555-123-4567', 'work', 'this is a note');
$Phone2 = Factory::phoneFactory('5559876543');
// don't validate or format as a U.S. number
$InternationalPhone = Factory::phoneFactory('0300 200 3300', '', '', false);
// update
$Phone->setPhoneNumber('5551239876');
// get information
echo $Phone->getPhoneNumber() . '<br>'; // "(555) 123-9876"
echo $Phone2->getPhoneNumber() . '<br>'; // "(555) 987-6543"
echo $InternationalPhone->getPhoneNumber() . '<br>'; // "0300 200 3300"
// validate (available statically)
$Phone->validateUsPhoneNumber('5551234567') . '<br>'; // true


<?php
ini_set('display_errors', 1);

// require the composer autoloader
require_once('vendor/autoload.php');

// reference the factory class to save typing
use Snap\Data\Factory;


// create an instance of \Snap\Data\Email (description and notes are optional)
// email address will be validated
$Email = Factory::emailFactory('john@email.com', 'work email', 'this is a note');
// update
$Email->setDescription('home email');
// get information
echo $Email->getEmail() . '<br>'; // "john@email.com"
echo $Email->getDescription() . '<br>'; // "home email"
// to string method
echo (string) $Email; // "john@email.com"
// validate (available statically)
$Email->validateEmail('jim@smith.com') . '<br>'; // true


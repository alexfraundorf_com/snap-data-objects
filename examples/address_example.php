<?php
ini_set('display_errors', 1);

// require the composer autoloader
require_once('vendor/autoload.php');

// reference the factory class to save typing
use Snap\Data\Factory;


// create an instance of \Snap\Data\Address (description and notes are optional)
$Address = Factory::addressFactory('123 Fake Street', 'Appleton', 'WI', '54915', 
    'home', 'this is a note');
// update 
$Address->setAddress1('555 Fake Street');
// add additional information
$Address->setAddress2('Apt. 3B');
// get information
echo $Address->getAddress1() . '<br>'; // "555 Fake Street"
echo $Address->getAddress2() . '<br>'; // "Apt. 3B"
// validate (available statically)
$Address->validateZipCode('90210') . '<br>'; // true

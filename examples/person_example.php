<?php
ini_set('display_errors', 1);

// require the composer autoloader
require_once('vendor/autoload.php');

// reference the factory class to save typing
use Snap\Data\Factory;


// create an instance of \Snap\Data\Person
$Person = Factory::personFactory();
// set some data
$Person
    ->setId('12345')
    ->setFirstName('Alex')
    ->setLastName('Fraundorf')
    ->buildFullName();
    ;
// get some data
echo $Person->getFirstName() . '<br>'; // "Alex"
echo $Person->getLastName() . '<br>'; // "Fraundorf"
echo $Person->getFullName() . '<br>'; // "Alex Fraundorf"

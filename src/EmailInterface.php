<?php
/**
 * Interface for the email data class.  
 * 
 * @package \Snap\Data
 * @author Alex Fraundorf - AlexFraundorf.com
 * @copyright (c) 2018, Alex Fraundorf and Snap Programming and Development LLC
 * @version 1.0.0 03/27/2018
 * @since 1.0.0 03/27/2018
 * @license MIT License http://www.opensource.org/licenses/mit-license.php
 */
namespace Snap\Data;

interface EmailInterface {

    

    /**
     * Constructor.
     * 
     * @param string $email_address
     * @param string $description (optional)
     * @param string $notes (optional)
     */
    public function __construct($email_address, $description = '', $notes = '');


    /**
     * Validate and set the email address.
     * 
     * @param string $email
     * @return \Snap\Data\EmailInterface
     */
    public function setEmail($email);
    
    
    /**
     * Return the email address.
     * 
     * @return string
     */
    public function getEmail();

    
    /**
     * Magic to string method.
     * 
     * @return string the email address
     */
    public function __toString();
    
    
    /**
     * Validate an email address and optionally throw an exception on failure.
     * 
     * @param (string) $email the email address to validate
     * @param (bool) $throw_exception if true an exception will be thrown on 
     *  failed validation
     * @return (bool) of validation
     * @throws \Snap\Data\Exception\InvalidEmailException
     */
    public static function validateEmail($email, $throw_exception = true);


    /**
     * Set the ID.
     * 
     * @param string $id
     * @return \Snap\Data\EmailInterface
     */
    public function setId($id);
    
    
    /**
     * Return the ID.
     * 
     * @return string
     */
    public function getId();

    
    /**
     * Set the description.
     * 
     * @param string $description
     * @return \Snap\Data\EmailInterface
     */
    public function setDescription($description);
    
    
    /**
     * Return the description.
     * 
     * @return string
     */
    public function getDescription();
    
    
    /**
     * Set the notes.
     * 
     * @param string $notes
     * @return \Snap\Data\EmailInterface
     */
    public function setNotes($notes);
    
    
    /**
     * Return the notes.
     * 
     * @return string
     */
    public function getNotes();
    
        
}

<?php
/**
 * The Snap address data class.  
 * 
 * @package \Snap\Data
 * @author Alex Fraundorf - AlexFraundorf.com
 * @copyright (c) 2018, Alex Fraundorf and Snap Programming and Development LLC
 * @version 1.0.0 03/27/2018
 * @since 1.0.0 03/15/2018
 * @license MIT License http://www.opensource.org/licenses/mit-license.php
 */
namespace Snap\Data;

require_once('DataAbstract.php');
require_once('AddressInterface.php');
use \Snap\Data\Exception;

class Address extends DataAbstract implements AddressInterface {
 
    /**
     *
     * @var string address line 1
     */
    protected $address1;
    
    /**
     *
     * @var string address line 2
     */
    protected $address2;
    
    /**
     *
     * @var string address line 3
     */
    protected $address3;
    
    /**
     *
     * @var string city
     */
    protected $city;
    
    /**
     *
     * @var string state or province
     */
    protected $state_province;
    
    /**
     *
     * @var string postal code
     */
    protected $postal_code;
    
    /**
     *
     * @var string region (or county or jurisdiction)
     */
    protected $region;
    
    /**
     *
     * @var string country
     */
    protected $country;
    
    

    /**
     * Constructor
     * 
     * @param string $address1 address line 1
     * @param string $city city
     * @param string $state_province state or province
     * @param string $postal_code postal code
     * @param string $description (optional)
     * @param string $notes (optional)
     * @version 1.0.0 03/15/2018
     * @since 1.0.0 03/15/2018
     */
    public function __construct($address1, $city, $state_province, $postal_code, $description = '', $notes = '') {
        $this
                ->setAddress1($address1)
                ->setCity($city)
                ->setStateProvince($state_province)
                ->setPostalCode($postal_code)
                ->setDescription($description)
                ->setNotes($notes)
                ;
    }


    /**
     * Set address line 1.
     * 
     * @param string $address1
     * @return \Snap\Data\Address
     * @version 1.0.0 03/22/2018
     * @since 1.0.0 03/15/2018
     */
    public function setAddress1($address1) {
        $this->address1 = trim((string) $address1);
        return $this;
    }
    
    
    /**
     * Return address line 1.
     * 
     * @return string
     * @version 1.0.0 03/14/2018
     * @since 1.0.0 03/14/2018
     */
    public function getAddress1() {
        if(isset($this->address1)) {
            return (string) $this->address1;
        }
        return '';
    }


    /**
     * Set address line 2.
     * 
     * @param string $address2
     * @return \Snap\Data\Address
     * @version 1.0.0 03/22/2018
     * @since 1.0.0 03/15/2018
     */
    public function setAddress2($address2) {
        $this->address2 = trim((string) $address2);
        return $this;
    }
    
    
    /**
     * Return address line 2.
     * 
     * @return string
     * @version 1.0.0 03/14/2018
     * @since 1.0.0 03/14/2018
     */
    public function getAddress2() {
        if(isset($this->address2)) {
            return (string) $this->address2;
        }
        return '';
    }


    /**
     * Set address line 3.
     * 
     * @param string $address3
     * @return \Snap\Data\Address
     * @version 1.0.0 03/22/2018
     * @since 1.0.0 03/15/2018
     */
    public function setAddress3($address3) {
        $this->address3 = trim((string) $address3);
        return $this;
    }
    
    
    /**
     * Return address line 3.
     * 
     * @return string
     * @version 1.0.0 03/14/2018
     * @since 1.0.0 03/14/2018
     */
    public function getAddress3() {
        if(isset($this->address3)) {
            return (string) $this->address3;
        }
        return '';
    }


    /**
     * Set the city.
     * 
     * @param string $city
     * @return \Snap\Data\Address
     * @version 1.0.0 03/22/2018
     * @since 1.0.0 03/15/2018
     */
    public function setCity($city) {
        $this->city = trim((string) $city);
        return $this;
    }
    
    
    /**
     * Return the city.
     * 
     * @return string
     * @version 1.0.0 03/14/2018
     * @since 1.0.0 03/14/2018
     */
    public function getCity() {
        if(isset($this->city)) {
            return (string) $this->city;
        }
        return '';
    }


    /**
     * Set the state or province.
     * 
     * @param string $state_province
     * @return \Snap\Data\Address
     * @version 1.0.0 03/22/2018
     * @since 1.0.0 03/15/2018
     */
    public function setStateProvince($state_province) {
        $this->state_province = trim((string) $state_province);
        return $this;
    }
    
    
    /**
     * Return the state or province.
     * 
     * @return string
     * @version 1.0.0 03/14/2018
     * @since 1.0.0 03/14/2018
     */
    public function getStateProvince() {
        if(isset($this->state_province)) {
            return (string) $this->state_province;
        }
        return '';
    }


    /**
     * Set the postal code.
     * 
     * @param string $postal_code
     * @return \Snap\Data\Address
     * @version 1.0.0 03/22/2018
     * @since 1.0.0 03/15/2018
     */
    public function setPostalCode($postal_code) {
        $this->postal_code = trim((string) $postal_code);
        return $this;
    }
    
    
    /**
     * Return the postal code.
     * 
     * @return string
     * @version 1.0.0 03/14/2018
     * @since 1.0.0 03/14/2018
     */
    public function getPostalCode() {
        if(isset($this->postal_code)) {
            return (string) $this->postal_code;
        }
        return '';
    }


    /**
     * Set the region (or county or jurisdiction).
     * 
     * @param string $region
     * @return \Snap\Data\Address
     * @version 1.0.0 03/15/2018
     * @since 1.0.0 03/15/2018
     */
    public function setRegion($region) {
        $this->region = (string) $region;
        return $this;
    }
    
    
    /**
     * Return the region.
     * 
     * @return string
     * @version 1.0.0 03/14/2018
     * @since 1.0.0 03/14/2018
     */
    public function getRegion() {
        if(isset($this->region)) {
            return (string) $this->region;
        }
        return '';
    }


    /**
     * Set the country.
     * 
     * @param string $country
     * @return \Snap\Data\Address
     * @version 1.0.0 03/15/2018
     * @since 1.0.0 03/15/2018
     */
    public function setCountry($country) {
        $this->country = (string) $country;
        return $this;
    }
    
    
    /**
     * Return the country.
     * 
     * @return string
     * @version 1.0.0 03/14/2018
     * @since 1.0.0 03/14/2018
     */
    public function getCountry() {
        if(isset($this->country)) {
            return (string) $this->country;
        }
        return '';
    }
    

    /**
     * Build and return the address as a string.
     * 
     * @param string $line_break character(s) to use as line break
     * @return string
     * @version 1.0.0 03/15/2018
     * @since 1.0.0 03/15/2018
     */
    public function toString($line_break = '<br>') {
        $output = '';
        if($this->getAddress1()) {
            $output .= $this->getAddress1() . $line_break;
        }
        if($this->getAddress2()) {
            $output .= $this->getAddress2() . $line_break;
        }
        if($this->getAddress3()) {
            $output .= $this->getAddress3() . $line_break;
        }
        if($this->getCity()) {
            $output .= $this->getCity();
        }
        if($this->getCity() && $this->getStateProvince()) {
            $output .= ', ';
        }
        if($this->getStateProvince()) {
            $output .= $this->getStateProvince();
        }
        if(($this->getCity() || $this->getStateProvince()) && $this->getPostalCode()) {
            $output .= ' ';
        }
        if($this->getPostalCode()) {
            $output .= $this->getPostalCode();
        }
        if($this->getCity() || $this->getStateProvince() || $this->getPostalCode()) {
            $output .= $line_break;
        }
        if($this->getRegion()) {
            $output .= $this->getRegion() . $line_break;
        }
        if($this->getCountry()) {
            $output .= $this->getCountry() . $line_break;
        }
        return rtrim($output, $line_break);
    }
    
    
    /**
     * Magic to string method.
     * 
     * @return string representation of the address
     * @version 1.0.0 03/15/2018
     * @since 1.0.0 03/15/2018
     */
    public function __toString() {
        return (string) $this->toString();
    }
    
    
    /**
     * Validate that the object has the minimum properties set to be an address 
     *  and optionally throw an exception on failure.
     * 
     * @param bool $throw_exception
     * @return boolean of validation
     * @throws \Snap\Data\Exception\InvalidAddressDataObjectException
     * @version 1.0.0 03/15/2018
     * @since 1.0.0 03/15/2018
     */
    public function validateMinimumAddress($throw_exception = true) {
        if(!$this->getAddress1()) {
            if($throw_exception) {
                throw new Exception\InvalidAddressDataObjectException('Address '
                        . 'must have a value for address1.', 1);
            }
            return false;
        }
        if(!$this->getCity()) {
            if($throw_exception) {
                throw new Exception\InvalidAddressDataObjectException('Address '
                        . 'must have a value for city.', 2);
            }
            return false;
        }
        if(!$this->getStateProvince()) {
            if($throw_exception) {
                throw new Exception\InvalidAddressDataObjectException('Address '
                        . 'must have a value for state/province.', 3);
            }
            return false;
        }
        if(!$this->getPostalCode()) {
            if($throw_exception) {
                throw new Exception\InvalidAddressDataObjectException('Address '
                        . 'must have a value for postal code.', 4);
            }
            return false;
        }
        return true;
    }


    /**
     * Validate a U.S. zip code and optionally throw an exception on failure.
     * 
     * Note: regex from http://www.ryanwright.me/cookbook/php/validation/zipcode
     * 
     * @param string $zip_code in 12345 or 12345-6789 formats
     * @param bool $throw_exception
     * @return boolean
     * @throws \Snap\Data\Exception\InvalidPostalCodeException
     * @version 1.0.0 03/22/2018
     * @since 1.0.0 03/15/2018
     */
    public static function validateZipCode($zip_code, $throw_exception = true) {
        if(!preg_match('#^\d{5}([\-]?\d{4})?$#', $zip_code)) {
            if($throw_exception) {
                throw new Exception\InvalidPostalCodeException('Zip code (' 
                        . $zip_code . ') is not valid.');
            }
            return false;
        }
        return true;
    }


    /**
     * Validate a Canadian postal code and optionally throw an exception on failure.
     * 
     * Note: regex from http://www.ryanwright.me/cookbook/php/validation/zipcode
     * 
     * @param string $postal_code
     * @param bool $throw_exception
     * @return boolean
     * @throws \Snap\Data\Exception\InvalidPostalCodeException
     * @version 1.0.0 03/22/2018
     * @since 1.0.0 03/15/2018
     */
    public static function validateCanadianPostalCode($postal_code, $throw_exception = true) {
        if(!preg_match('/^[ABCEGHJKLMNPRSTVXY]\d[ABCEGHJ-NPRSTV-Z][ ]?\d[ABCEGHJ-NPRSTV-Z]\d$/', $postal_code)) {
            if($throw_exception) {
                throw new Exception\InvalidPostalCodeException('Canadian postal code (' 
                        . $postal_code . ') is not valid.');
            }
            return false;
        }
        return true;
    }


    /**
     * Validate a Great Britain postal code and optionally throw an exception on failure.
     * 
     * Note: regex from http://www.ryanwright.me/cookbook/php/validation/zipcode
     * 
     * @param string $postal_code
     * @param bool $throw_exception
     * @return boolean
     * @throws \Snap\Data\Exception\InvalidPostalCodeException
     * @version 1.0.0 03/22/2018
     * @since 1.0.0 03/15/2018
     */
    public static function validateGreatBritainPostalCode($postal_code, $throw_exception = true) {
        if(!preg_match('/^GIR[ ]?0AA|((AB|AL|B|BA|BB|BD|BH|BL|BN|BR|BS|BT|CA|CB|CF|CH|CM|CO|CR|CT|CV|CW|DA|DD|DE|DG|DH|DL|DN|DT|DY|E|EC|EH|EN|EX|FK|FY|G|GL|GY|GU|HA|HD|HG|HP|HR|HS|HU|HX|IG|IM|IP|IV|JE|KA|KT|KW|KY|L|LA|LD|LE|LL|LN|LS|LU|M|ME|MK|ML|N|NE|NG|NN|NP|NR|NW|OL|OX|PA|PE|PH|PL|PO|PR|RG|RH|RM|S|SA|SE|SG|SK|SL|SM|SN|SO|SP|SR|SS|ST|SW|SY|TA|TD|TF|TN|TQ|TR|TS|TW|UB|W|WA|WC|WD|WF|WN|WR|WS|WV|YO|ZE)(\d[\dA-Z]?[ ]?\d[ABD-HJLN-UW-Z]{2}))|BFPO[ ]?\d{1,4}$/', $postal_code)) {
            if($throw_exception) {
                throw new Exception\InvalidPostalCodeException('Great Britain postal '
                        . 'code (' . $postal_code . ') is not valid.');
            }
            return false;
        }
        return true;
    }


    /**
     * Validate a postal code with custom regex and optionally throw an exception on failure.
     * 
     * Note: Find regex at http://www.ryanwright.me/cookbook/php/validation/zipcode
     * 
     * @param string $postal_code
     * @param string $regex
     * @param bool $throw_exception
     * @return boolean
     * @throws \Snap\Data\Exception\InvalidPostalCodeException
     * @version 1.0.0 03/22/2018
     * @since 1.0.0 03/15/2018
     */
    public static function validatePostalCodeCustomRegex($postal_code, $regex, $throw_exception = true) {
        if(!preg_match($regex, $postal_code)) {
            if($throw_exception) {
                throw new Exception\InvalidPostalCodeException('Postal code (' 
                        . $postal_code . ') is not valid.');
            }
            return false;
        }
        return true;
    }

    
}

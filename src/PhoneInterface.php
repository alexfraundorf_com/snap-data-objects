<?php
/**
 * Interface for the phone data class.  
 * 
 * @package \Snap\Data
 * @author Alex Fraundorf - AlexFraundorf.com
 * @copyright (c) 2018, Alex Fraundorf and Snap Programming and Development LLC
 * @version 1.0.0 03/27/2018
 * @since 1.0.0 03/27/2018
 * @license MIT License http://www.opensource.org/licenses/mit-license.php
 */
namespace Snap\Data;

interface PhoneInterface {
    

    /**
     * Constructor.
     * 
     * Note: Setting a phone number with an extension will result in a failed 
     *  validation.
     * 
     * @param string $phone_number
     * @param string $description (optional)
     * @param string $notes (optional)
     * @param bool $validate_as_us_phone_number validate as a U.S. phone number (optional)
     */
    public function __construct($phone_number, $description = '', $notes = '', 
            $validate_as_us_phone_number = true);


    /**
     * Validate, format and set the phone number.
     * 
     * Note: Setting a phone number with an extension will result in a failed 
     *  validation.
     * 
     * @param string $phone_number
     * @param bool $validate_as_us_phone_number validate as a U.S. phone number
     * @return \Snap\Data\PhoneInterface
     */
    public function setPhoneNumber($phone_number, $validate_as_us_phone_number = true);
    
    
    /**
     * Return the phone number.
     * 
     * @return string
     */
    public function getPhoneNumber();

    
    /**
     * Magic to string method.
     * 
     * @return string the phone number
     */
    public function __toString();
    
    
    /**
     * Validate a U.S. phone number and optionally throw an exception on failure.
     * 
     * @param string $phone_number
     * @param bool $throw_exception
     * @return boolean of validation
     * @throws \Snap\Data\Exception\InvalidPhoneException
     */
    public static function validateUsPhoneNumber($phone_number, $throw_exception = true);
    
    
    /**
     * Format the phone number is a U.S. friendly format: (123) 456-7890 x1234.
     * 
     * @param string $phone_number
     * @return string
     * @throws \InvalidArgumentException
     */
    public static function formatPhoneNumber($phone_number);


    /**
     * Set the ID.
     * 
     * @param string $id
     * @return \Snap\Data\PhoneInterface
     */
    public function setId($id);
    
    
    /**
     * Return the ID.
     * 
     * @return string
     */
    public function getId();

    
    /**
     * Set the description.
     * 
     * @param string $description
     * @return \Snap\Data\PhoneInterface
     */
    public function setDescription($description);
    
    
    /**
     * Return the description.
     * 
     * @return string
     */
    public function getDescription();
    
    
    /**
     * Set the notes.
     * 
     * @param string $notes
     * @return \Snap\Data\PhoneInterface
     */
    public function setNotes($notes);
    
    
    /**
     * Return the notes.
     * 
     * @return string
     */
    public function getNotes();
    
        
}

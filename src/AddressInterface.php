<?php
/**
 * Interface for the address data class.
 * 
 * @package \Snap\Data
 * @author Alex Fraundorf - AlexFraundorf.com
 * @copyright (c) 2018, Alex Fraundorf and Snap Programming and Development LLC
 * @version 1.0.0 03/27/2018
 * @since 1.0.0 03/27/2018
 * @license MIT License http://www.opensource.org/licenses/mit-license.php
 */
namespace Snap\Data;

interface AddressInterface {
 
    
    /**
     * Constructor
     * 
     * @param string $address1 address line 1
     * @param string $city city
     * @param string $state_province state or province
     * @param string $postal_code postal code
     * @param string $description (optional)
     * @param string $notes (optional)
     */
    public function __construct($address1, $city, $state_province, $postal_code, $description = '', $notes = '');


    /**
     * Set address line 1.
     * 
     * @param string $address1
     * @return \Snap\Data\AddressInterface
     */
    public function setAddress1($address1);
    
    
    /**
     * Return address line 1.
     * 
     * @return string
     */
    public function getAddress1();


    /**
     * Set address line 2.
     * 
     * @param string $address2
     * @return \Snap\Data\AddressInterface
     */
    public function setAddress2($address2);
    
    
    /**
     * Return address line 2.
     * 
     * @return string
     */
    public function getAddress2();


    /**
     * Set address line 3.
     * 
     * @param string $address3
     * @return \Snap\Data\AddressInterface
     */
    public function setAddress3($address3);
    
    
    /**
     * Return address line 3.
     * 
     * @return string
     */
    public function getAddress3();


    /**
     * Set the city.
     * 
     * @param string $city
     * @return \Snap\Data\AddressInterface
     */
    public function setCity($city);
    
    
    /**
     * Return the city.
     * 
     * @return string
     */
    public function getCity();


    /**
     * Set the state or province.
     * 
     * @param string $state_province
     * @return \Snap\Data\AddressInterface
     */
    public function setStateProvince($state_province);
    
    
    /**
     * Return the state or province.
     * 
     * @return string
     */
    public function getStateProvince();


    /**
     * Set the postal code.
     * 
     * @param string $postal_code
     * @return \Snap\Data\AddressInterface
     */
    public function setPostalCode($postal_code);
    
    
    /**
     * Return the postal code.
     * 
     * @return string
     */
    public function getPostalCode();


    /**
     * Set the region (or county or jurisdiction).
     * 
     * @param string $region
     * @return \Snap\Data\AddressInterface
     */
    public function setRegion($region);
    
    
    /**
     * Return the region.
     * 
     * @return string
     */
    public function getRegion();


    /**
     * Set the country.
     * 
     * @param string $country
     * @return \Snap\Data\AddressInterface
     */
    public function setCountry($country);
    
    
    /**
     * Return the country.
     * 
     * @return string
     */
    public function getCountry();
    

    /**
     * Build and return the address as a string.
     * 
     * @param string $line_break character(s) to use as line break
     * @return string
     */
    public function toString($line_break = '<br>');
    
    
    /**
     * Magic to string method.
     * 
     * @return string representation of the address
     */
    public function __toString();
    
    
    /**
     * Validate that the object has the minimum properties set to be an address 
     *  and optionally throw an exception on failure.
     * 
     * @param bool $throw_exception
     * @return boolean of validation
     * @throws \Snap\Data\Exception\InvalidAddressDataObjectException
     */
    public function validateMinimumAddress($throw_exception = true);


    /**
     * Validate a U.S. zip code and optionally throw an exception on failure.
     * 
     * Note: regex from http://www.ryanwright.me/cookbook/php/validation/zipcode
     * 
     * @param string $zip_code in 12345 or 12345-6789 formats
     * @param bool $throw_exception
     * @return boolean
     * @throws \Snap\Data\Exception\InvalidPostalCodeException
     */
    public static function validateZipCode($zip_code, $throw_exception = true);


    /**
     * Validate a Canadian postal code and optionally throw an exception on failure.
     * 
     * @param string $postal_code
     * @param bool $throw_exception
     * @return boolean
     * @throws \Snap\Data\Exception\InvalidPostalCodeException
     */
    public static function validateCanadianPostalCode($postal_code, $throw_exception = true);


    /**
     * Validate a Great Britain postal code and optionally throw an exception on failure.
     * 
     * @param string $postal_code
     * @param bool $throw_exception
     * @return boolean
     * @throws \Snap\Data\Exception\InvalidPostalCodeException
     */
    public static function validateGreatBritainPostalCode($postal_code, $throw_exception = true);


    /**
     * Validate a postal code with custom regex and optionally throw an exception on failure.
     * 
     * @param string $postal_code
     * @param string $regex
     * @param bool $throw_exception
     * @return boolean
     * @throws \Snap\Data\Exception\InvalidPostalCodeException
     */
    public static function validatePostalCodeCustomRegex($postal_code, $regex, $throw_exception = true);


    /**
     * Set the ID.
     * 
     * @param string $id
     * @return \Snap\Data\AddressInterface
     */
    public function setId($id);
    
    
    /**
     * Return the ID.
     * 
     * @return string
     */
    public function getId();

    
    /**
     * Set the description.
     * 
     * @param string $description
     * @return \Snap\Data\AddressInterface
     */
    public function setDescription($description);
    
    
    /**
     * Return the description.
     * 
     * @return string
     */
    public function getDescription();
    
    
    /**
     * Set the notes.
     * 
     * @param string $notes
     * @return \Snap\Data\AddressInterface
     */
    public function setNotes($notes);
    
    
    /**
     * Return the notes.
     * 
     * @return string
     */
    public function getNotes();
    
        
}

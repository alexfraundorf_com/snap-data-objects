<?php
/**
 * The Snap credit card data class.  
 * 
 * @package \Snap\Data
 * @author Alex Fraundorf - AlexFraundorf.com
 * @copyright (c) 2018, Alex Fraundorf and Snap Programming and Development LLC
 * @version 1.0.0 03/27/2018
 * @since 1.0.0 03/15/2018
 * @license MIT License http://www.opensource.org/licenses/mit-license.php
 */
namespace Snap\Data;

require_once('DataAbstract.php');
require_once('CreditCardInterface.php');
use \Snap\Data\Exception;

class CreditCard extends DataAbstract implements CreditCardInterface {
 
    /**
     *
     * @var string card holder name
     */
    protected $name;
    
    /**
     *
     * @var string credit card number (only numbers)
     */
    protected $card_number;
    
    /**
     *
     * @var string two digit month expiration (with leading zero if needed)
     */
    protected $expiration_month;
    
    /**
     *
     * @var string two digit year expiration
     */
    protected $expiration_year;
    
    /**
     *
     * @var string ccv/cvv/cvc verification code
     */
    protected $verification_code;
    
    /**
     *
     * @var \Snap\Data\Address
     */
    protected $BillingAddress;
    
    /**
     *
     * @var string billing address postal code
     */
    protected $postal_code;
    
    

    /**
     * Constructor
     * 
     * @param string $card_number
     * @param string $expiration_month two digit numeric month
     * @param string $expiration_year two or four digit numeric year
     * @param string $verification_code card verification code (optional)
     * @param string $billing_postal_code billing postal code (optional)
     * @param string $description description (optional)
     * @param string $notes notes (optional)
     * @version 1.0.0 03/22/2018
     * @since 1.0.0 03/15/2018
     */
    public function __construct($card_number, $expiration_month, $expiration_year, 
            $verification_code = '', $billing_postal_code = '', $description = '', $notes = '') {
        $this->setNumber($card_number);
        $this->setExpirationMonth($expiration_month);
        $this->setExpirationYear($expiration_year);
        $this->validateExpirationDate($this->expiration_month, $this->expiration_year);
        $this->setVerificationCode($verification_code);
        $this->setPostalCode($billing_postal_code);
        $this->description = (string) $description;
        $this->notes = (string) $notes;
    }


    /**
     * Validate, format and set the card number.
     * 
     * @param string $card_number
     * @param bool $validate if true (default) the card number will be validated
     * @return \Snap\Data\CreditCard
     * @throws \Snap\Data\Exception\InvalidCardNumberException
     * @version 1.0.0 03/22/2018
     * @since 1.0.0 03/15/2018
     */
    public function setNumber($card_number, $validate = true) {
        $formatted = (string) preg_replace("/[^0-9]/", '', $card_number);
        if($validate) {
            if(!trim($card_number)) {
                throw new Exception\InvalidCardNumberException('You must provide '
                        . 'a card number.');
            }
            $this->validateCardNumber($formatted);
        }
        $this->card_number = $formatted;
        return $this;
    }
    
    
    /**
     * Return the card number.
     * 
     * @return string
     * @version 1.0.0 03/15/2018
     * @since 1.0.0 03/15/2018
     */
    public function getNumber() {
        if(isset($this->card_number)) {
            return (string) $this->card_number;
        }
        return '';
    }


    /**
     * Validate, format and set the expiration month.
     * 
     * @param string $month two digit numeric month.
     * @return \Snap\Data\CreditCard
     * @throws \InvalidArgumentException
     * @version 1.0.0 03/15/2018
     * @since 1.0.0 03/15/2018
     */
    public function setExpirationMonth($month) {
        // validate
        $int_month = (int) $month;
        if($int_month < 1 || $int_month > 12) {
            throw new \InvalidArgumentException('Invalid expriration month: ' . $month);
        } 
        // format and set
        if($int_month >= 10) {
            $this->expiration_month = (string) $int_month;
        }
        else {
            $this->expiration_month = '0' . (string) $int_month;
        }
        return $this;
    }
    
    
    /**
     * Return the expiration month.
     * 
     * @return string
     * @version 1.0.0 03/15/2018
     * @since 1.0.0 03/15/2018
     */
    public function getExpirationMonth() {
        if(isset($this->expiration_month)) {
            return (string) $this->expiration_month;
        }
        return '';
    }

    
    /**
     * Validate, format and set the expiration year.
     * 
     * @param string $year two or four digit numeric year.
     * @return \Snap\Data\CreditCard
     * @throws \Snap\Data\Exception\ExpiredCardException
     * @throws \OutOfRangeException
     * @version 1.0.0 03/27/2018
     * @since 1.0.0 03/15/2018
     */
    public function setExpirationYear($year) {
        // convert 4 digit year to 2 digit year
        if((int) $year > 2099) {
            $year = (string) $year - 2100;
        } 
        elseif((int) $year > 99) {
            $year = (string) $year - 2000;
        }
        else {
            $year = (string) $year;
        }
        // validate
        if((int) $year < (int) date('y')) {
            throw new Exception\ExpiredCardException('The credit card is expired.');
        }
        elseif((int) $year > 99) {
            throw new \OutOfRangeException('Invalid expiration year: ' . $year);
        }
        // set
        $this->expiration_year = (string) $year;
        return $this;
    }
    
    
    /**
     * Return the expiration year.
     * 
     * @return string
     * @version 1.0.0 03/15/2018
     * @since 1.0.0 03/15/2018
     */
    public function getExpirationYear() {
        if(isset($this->expiration_year)) {
            return (string) $this->expiration_year;
        }
        return '';
    }

    
    /**
     * Validate and set the verification_code.
     * 
     * @param string $code 3 or 4 digit numeric code
     * @return \Snap\Data\CreditCard
     * @throws \InvalidArgumentException
     * @version 1.0.0 03/15/2018
     * @since 1.0.0 03/15/2018
     */
    public function setVerificationCode($code) {
        // validate
        if(!(string) $code) {
            throw new \InvalidArgumentException('Invalid verification code: ' 
                    . $code);
        }
        // set
        $this->verification_code = (string) $code;
        return $this;
    }
    
    
    /**
     * Return the verification code.
     * 
     * @return string
     * @version 1.0.0 03/15/2018
     * @since 1.0.0 03/15/2018
     */
    public function getVerificationCode() {
        if(isset($this->verification_code)) {
            return (string) $this->verification_code;
        }
        return '';
    }

    
    /**
     * Set the billing postal code.
     * 
     * @param string $postal_code
     * @return \Snap\Data\CreditCard
     * @version 1.0.0 03/15/2018
     * @since 1.0.0 03/15/2018
     */
    public function setPostalCode($postal_code) {
        $this->postal_code = (string) $postal_code;
        return $this;
    }
    
    
    /**
     * Return the billing postal code.
     * 
     * @return string
     * @version 1.0.0 03/15/2018
     * @since 1.0.0 03/15/2018
     */
    public function getPostalCode() {
        if(isset($this->postal_code)) {
            return (string) $this->postal_code;
        }
        return '';
    }

    
    /**
     * Set the billing address object and the postal code.
     * 
     * @param \Snap\Data\Address $BillingAddress
     * @return \Snap\Data\CreditCard
     * @version 1.0.0 03/15/2018
     * @since 1.0.0 03/15/2018
     */
    public function setBillingAddressObject(Address $BillingAddress) {
        $this->BillingAddress = $BillingAddress;
        $this->setPostalCode($BillingAddress->getPostalCode());
        return $this;
    }
    
    
    /**
     * Return the billing address object or null if there is none.
     * 
     * @return \Snap\Data\Address|null
     * @version 1.0.0 03/15/2018
     * @since 1.0.0 03/15/2018
     */
    public function getBillingAddressObject() {
        return $this->BillingAddress;
    }
    
    
    /**
     * Validate that the minimum card data is set and optionally throw an 
     *  exception if it is not.
     * 
     * @param bool $throw_exception
     * @return boolean
     * @throws \Snap\Data\Exception\InvalidCardDataObjectException
     * @version 1.0.0 03/22/2018
     * @since 1.0.0 03/15/2018
     */
    public function validateMinimumDataSet($throw_exception = true) {
        if(!$this->getNumber()) {
            if($throw_exception) {
                throw new Exception\InvalidCardDataObjectException('Card number '
                        . 'must have a value.', 1);
            }
            return false;
        }
        if(!$this->getExpirationMonth()) {
            if($throw_exception) {
                throw new Exception\InvalidCardDataObjectException('Expiration '
                        . 'month must have a value.', 2);
            }
            return false;
        }
        if(!$this->getExpirationYear()) {
            if($throw_exception) {
                throw new Exception\InvalidCardDataObjectException('Expiration '
                        . 'year must have a value.', 3);
            }
            return false;
        }
        try {
            $this->validateExpirationDate($this->getExpirationMonth(), $this->expiration_year);
        } catch (\Exception $ex) {
            if($throw_exception) {
                throw new Exception\ExpiredCardException('Expired card: ' 
                        . $ex->getMessage());
            }
            return false;
        }
        if(!$this->getVerificationCode()) {
            if($throw_exception) {
                throw new Exception\InvalidCardDataObjectException('Verification '
                        . 'code must have a value.', 4);
            }
            return false;
        }
        if(!$this->getPostalCode()) {
            if($throw_exception) {
                throw new Exception\InvalidCardDataObjectException('Postal code '
                        . 'or billing address object must have a value.', 5);
            }
            return false;
        }
        return true;
    }
    
    
    /**
     * Validate a card number according to the Luhn algorithm.
     * 
     * Note: From \Omnipay\Common\Helper.php
     *
     * @param string $card_number The card number to validate
     * @param bool throw exception
     * @return boolean of card number validation
     * @throws \Snap\Data\Exception\InvalidCardNumberException
     * @version 1.0.0 03/22/2018
     * @since 1.0.0 03/15/2018
     */
    public static function validateCardNumber($card_number, $throw_exception = true) {
        $str = '';
        foreach (array_reverse(str_split($card_number)) as $i => $c) {
            $str .= $i % 2 ? $c * 2 : $c;
        }
        $result = (bool) (array_sum(str_split($str)) % 10 === 0);
        if(!$result && $throw_exception) {
            throw new Exception\InvalidCardNumberException('The provided card '
                    . 'number is invalid (Failed Luhn).');
        }
        return $result;
    }
    
    
    /**
     * Validate a provided expiration month and year and optionally throw an 
     *  exception on failure.
     * 
     * @param string $month two digit numeric month
     * @param string $year two or four digit numeric year
     * @param bool $throw_exception 
     * @return boolean
     * @throws \InvalidArgumentException
     * @throws \Snap\Data\Exception\ExpiredCardException
     * @throws \OutOfRangeException
     * @version 1.0.0 03/27/2018
     * @since 1.0.0 03/15/2018
     */
    public static function validateExpirationDate($month, $year, $throw_exception = true) {
        // convert 4 digit year to 2 digit year
        if((int) $year > 2099) {
            $year = (int) $year - 2100;
        } 
        elseif((int) $year > 99) {
            $year = (int) $year - 2000;
        }
        else {
            $year = (int) $year;
        }
        // validate month
        if((int) $month < 1 || (int) $month > 12) {
            if($throw_exception) {
                throw new \InvalidArgumentException('Invalid expriration month: ' . $month);
            }
            else {
                return false;
            }
        } 
        // make sure the date is not expired
        if($year < (int) date('y') || 
                ((int) $year === (int) date('y') && (int) $month < (int) date('m'))
                ) {
            if($throw_exception) {
                throw new Exception\ExpiredCardException('The credit card is expired.');
            }
            else {
                return false;
            }
        }
        // validate year
        elseif($year > 99) {
            if($throw_exception) {
                throw new \OutOfRangeException('The expiration year is invalid.');
            }
            else {
                return false;
            }
        }
        return true;
    }
    
    
}

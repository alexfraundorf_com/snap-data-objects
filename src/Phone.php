<?php
/**
 * The Snap phone data class.  
 * 
 * @package \Snap\Data
 * @author Alex Fraundorf - AlexFraundorf.com
 * @copyright (c) 2018, Alex Fraundorf and Snap Programming and Development LLC
 * @version 1.0.0 03/27/2018
 * @since 1.0.0 03/14/2018
 * @license MIT License http://www.opensource.org/licenses/mit-license.php
 */
namespace Snap\Data;

require_once('DataAbstract.php');
require_once('PhoneInterface.php');
use Snap\Data\Exception;

class Phone extends DataAbstract implements PhoneInterface {
 
    /**
     *
     * @var string phone number
     */
    protected $phone_number;
    
    

    /**
     * Constructor.
     * 
     * Note: Setting a phone number with an extension will result in a failed 
     *  validation.
     * 
     * @param string $phone_number
     * @param string $description (optional)
     * @param string $notes (optional)
     * @param bool $validate_and_format_as_us_phone_number 
     * @version 1.0.0 03/27/2018
     * @since 1.0.0 03/14/2018
     */
    public function __construct($phone_number, $description = '', $notes = '', 
            $validate_and_format_as_us_phone_number = true) {
        $this->setPhoneNumber($phone_number, $validate_and_format_as_us_phone_number);
        $this->description = (string) $description;
        $this->notes = (string) $notes;
    }


    /**
     * Validate, format and set the phone number.
     * 
     * Note: Setting a phone number with an extension will result in a failed 
     *  validation.
     * 
     * @param string $phone_number
     * @param bool $validate_and_format_as_us_phone_number 
     * @return \Snap\Data\Phone
     * @version 1.0.0 03/27/2018
     * @since 1.0.0 03/14/2018
     */
    public function setPhoneNumber($phone_number, $validate_and_format_as_us_phone_number = true) {
        if($validate_and_format_as_us_phone_number) {
            $formatted = $this->formatPhoneNumber($phone_number);
            $this->validateUsPhoneNumber($formatted);
            $this->phone_number = $formatted;
        }
        else {
            $this->phone_number = trim($phone_number);
        }
        return $this;
    }
    
    
    /**
     * Return the phone number.
     * 
     * @return string
     * @version 1.0.0 03/14/2018
     * @since 1.0.0 03/14/2018
     */
    public function getPhoneNumber() {
        return (string) $this->phone_number;
    }

    
    /**
     * Magic to string method.
     * 
     * @return string the phone number
     * @version 1.0.0 03/15/2018
     * @since 1.0.0 03/15/2018
     */
    public function __toString() {
        return (string) $this->getPhoneNumber();
    }
    
    
    /**
     * Format the phone number is a U.S. friendly format: (123) 456-7890 x1234.
     * 
     * Note: regex from https://ericholmes.ca/php-phone-number-validation-revisited/
     * 
     * @param string $phone_number
     * @param bool $throw_exception
     * @return boolean of validation
     * @throws \Snap\Data\Exception\InvalidPhoneException
     * @version 1.0.0 03/27/2018
     * @since 1.0.0 03/15/2018
     */
    public static function validateUsPhoneNumber($phone_number, $throw_exception = true) {
        if(preg_match('/^(\d[\s-]?)?[\(\[\s-]{0,2}?\d{3}[\)\]\s-]{0,2}?\d{3}[\s-]?\d{4}$/i', $phone_number)) {
            return true;
        }
        if($throw_exception) {
            throw new Exception\InvalidPhoneException('Phone number ('. $phone_number 
                    . ') is not a valid U.S. phone number.');
        }
        return false;
    }
    
    
    /**
     * Format the phone number is a U.S. friendly format.
     * 
     * @param string $phone_number
     * @return string
     * @throws \InvalidArgumentException
     * @version 1.0.0 03/15/2018
     * @since 1.0.0 03/15/2018
     */
    public static function formatPhoneNumber($phone_number) {
        if(!$phone_number) {
            throw new \InvalidArgumentException('Missing required param $phone_number');
        }
        // initialize the output
        $output = '';
        // strip out everything except numbers
        $stripped_phone_number = preg_replace("/[^x0-9]/", "", strtolower(trim((string) $phone_number)));
        // create an array of the phone number characters
        $pieces = str_split($stripped_phone_number);
        // if there is a leading 1
        if((int) $pieces[0] === 1) {
            $output .= '1 ';
            array_shift($pieces);
        }
        // format the rest of the number
        // area code
        $output .= '(' . $pieces[0] . $pieces[1] . $pieces[2] . ') '
        // local prefix
        . $pieces[3] . $pieces[4] . $pieces[5]
        // last 4
        . '-' . $pieces[6] . $pieces[7] . $pieces[8] . $pieces[9];
        // optional extension
        if(isset($pieces[10])) {
            $output .= ' x';
            $extension = array_slice($pieces, 10);
            // remove the "x" character if it is there
            if($extension[0] === 'x') {
                array_shift($extension);
            }
            // add on the rest
            foreach($extension as $ext) {
                $output .= $ext;
            }
        }
        // done
        return (string) $output;        
    }

    
}

<?php
/**
 * Interface for the person data class.  
 * 
 * @package \Snap\Data
 * @author Alex Fraundorf - AlexFraundorf.com
 * @copyright (c) 2018, Alex Fraundorf and Snap Programming and Development LLC
 * @version 1.0.0 03/27/2018
 * @since 1.0.0 03/27/2018
 * @license MIT License http://www.opensource.org/licenses/mit-license.php
 */
namespace Snap\Data;

interface PersonInterface {
    
    
    /**
     * Constructor
     * 
     * @param array $data optional array of data to set as object properties.
     * @param string $description (optional)
     * @param string $notes (optional)
     */
    public function __construct(array $data = [], $description = '', $notes = '');
    
    
    /**
     * Set the first name.
     * 
     * @param string $first_name
     * @return \Snap\Data\PersonInterface
     */
    public function setFirstName($first_name);
    
    
    /**
     * Return the first name.
     * 
     * @return string
     */
    public function getFirstName();
    
    
    /**
     * Set the middle name.
     * 
     * @param string $middle_name
     * @return \Snap\Data\PersonInterface
     */
    public function setMiddleName($middle_name);
    
    
    /**
     * Return the middle name.
     * 
     * @return string
     */
    public function getMiddleName();
    
    
    /**
     * Set the last name.
     * 
     * @param string $last_name
     * @return \Snap\Data\PersonInterface
     */
    public function setLastName($last_name);
    
    
    /**
     * Return the last name.
     * 
     * @return string
     */
    public function getLastName();
    
    
    /**
     * Set the full name.
     * 
     * @param string $full_name
     * @return \Snap\Data\PersonInterface
     */
    public function setFullName($full_name);
    
    
    /**
     * Build and set the full name from the set first, middle, last names and 
     *  suffix.
     * 
     * @return \Snap\Data\PersonInterface
     */
    public function buildFullName();
    
    
    /**
     * Return the full name or an empty string if it has not been built.
     * 
     * @return string
     */
    public function getFullName();
    
    
    /**
     * Set the salutation.
     * 
     * @param string $salutation
     * @return \Snap\Data\PersonInterface
     */
    public function setSalutation($salutation);
    
    
    /**
     * Return the salutation.
     * 
     * @return string
     */
    public function getSalutation();
    
    
    /**
     * Set the suffix.
     * 
     * @param string $suffix
     * @return \Snap\Data\PersonInterface
     */
    public function setSuffix($suffix);
    
    
    /**
     * Return the suffix.
     * 
     * @return string
     */
    public function getSuffix();
    
    
    /**
     * Set the title.
     * 
     * @param string $title
     * @return \Snap\Data\PersonInterface
     */
    public function setTitle($title);
    
    
    /**
     * Return the title.
     * 
     * @return string
     */
    public function getTitle();
    
    
    /**
     * Set the organization name.
     * 
     * @param string $name
     * @return \Snap\Data\PersonInterface
     */
    public function setOrganizationName($name);
    
    
    /**
     * Return the organization name.
     * 
     * @return string
     */
    public function getOrganizationName();
    
    
    /**
     * Set the serial/social security number.
     * 
     * Warning: If you are storing this data, make sure that you encrypt it!
     * 
     * @param string $serial_number
     * @return \Snap\Data\PersonInterface
     */
    public function setSerialNumber($serial_number);
    
    
    /**
     * Return the serial/social security number.
     * 
     * Warning: If you are storing this data, make sure that you encrypt it!
     * 
     * @return string
     */
    public function getSerialNumber();
    
    
    /**
     * Add an email address.
     * 
     * @param string $email_address
     * @param string $description (optional)
     * @param string $notes (optional)
     * @return \Snap\Data\PersonInterface
     */
    public function addEmailAddress($email_address, $description = '', $notes = '');
    
    
    /**
     * Add an email address object.
     * 
     * @param \Snap\Data\EmailInterface $Email
     * @return \Snap\Data\PersonInterface
     */
    public function addEmailAddressObject(EmailInterface $Email);
    
    
    /**
     * Return an array of email objects.
     * 
     * @return array
     */
    public function getEmailAddresses();
    
    
    /**
     * Return an email object by its key. If the key is not found, an exception 
     *  will be thrown or null will be returned.
     * 
     * @param (int) $key
     * @param (bool) $throw_exception
     * @return \Snap\Data\Email|null
     * @throws \Snap\Data\Exception\EmailDoesNotExistException
     */
    public function getEmailAddressObject($key = 0, $throw_exception = true);
    
    
    /**
     * Return an email address by its key. If the key does not exist an exception 
     *  will be thrown or an empty string will be returned.
     * 
     * @param (int) $key
     * @param (bool) $throw_exception
     * @return (string) 
     * @throws \Snap\Data\Exception\EmailDoesNotExistException
     */
    public function getEmailAddress($key = 0, $throw_exception = true);
    
    
    /**
     * Return an email object by its description. If not found, an exception 
     *  will be thrown or null will be returned.
     * 
     * @param (string) $description
     * @param (bool) $throw_exception
     * @return \Snap\Data\Email|null
     * @throws \Snap\Data\Exception\EmailDoesNotExistException
     */
    public function getEmailAddressObjectByDescription($description, $throw_exception = true);
    
    
    /**
     * Add a phone number.
     * 
     * @param string $phone_number
     * @param string $description (optional)
     * @param string $notes (optional)
     * @return \Snap\Data\PersonInterface
     */
    public function addPhoneNumber($phone_number, $description = '', $notes = '');
    
    
    /**
     * Add a phone number object.
     * 
     * @param \Snap\Data\PhoneInterface $Phone
     * @return \Snap\Data\PersonInterface
     */
    public function addPhoneNumberObject(PhoneInterface $Phone);
    
    
    /**
     * Return an array of phone number objects.
     * 
     * @return array
     */
    public function getPhoneNumbers();
    
    
    /**
     * Return a phone number object by its key. If not found an exception will 
     *  be thrown or null will be returned.
     * 
     * @param (int) $key
     * @param (bool) $throw_exception
     * @return \Snap\Data\Phone|null
     * @throws \Snap\Data\Exception\PhoneDoesNotExistException
     */
    public function getPhoneNumberObject($key = 0, $throw_exception = true);
    
    
    /**
     * Return a phone number by its key. If not found an exception will be 
     *  thrown or an empty string will be returned.
     * 
     * @param (int) $key
     * @param (bool) $throw_exception
     * @return (string) 
     * @throws \Snap\Data\Exception\PhoneDoesNotExistException
     */
    public function getPhoneNumber($key = 0, $throw_exception = true);
    
    
    /**
     * Return a phone number object by its description. If not found an exception 
     *  will be thrown or null will be returned.
     * 
     * @param (string) $description
     * @param (bool) $throw_exception
     * @return \Snap\Data\Phone|null
     * @throws \Snap\Data\Exception\PhoneDoesNotExistException
     */
    public function getPhoneNumberObjectByDescription($description, $throw_exception = true);
    
    
    /**
     * Add an address.
     * 
     * @param string $address1 address line 1
     * @param string $city city
     * @param string $state_province state or province
     * @param string $postal_code postal code
     * @param string $description (optional)
     * @param string $notes (optional)
     * @return \Snap\Data\PersonInterface
     */
    public function addAddress($address1, $city, $state_province, $postal_code, 
            $description = '', $notes = '');
    
    
    /**
     * Add an address object.
     * 
     * @param \Snap\Data\AddressInterface $Address
     * @return \Snap\Data\PersonInterface
     */
    public function addAddressObject(AddressInterface $Address);
    
    
    /**
     * Return an array of address objects.
     * 
     * @return array
     */
    public function getAddresses();
    
    
    /**
     * Return an address object by its key. If it does not exist an exception 
     *  will be thrown or null will be returned.
     * 
     * @param (int) $key
     * @param (bool) $throw_exception
     * @return \Snap\Data\Address|null
     * @throws \Snap\Data\Exception\AddressDoesNotExistException
     */
    public function getAddressObject($key = 0, $throw_exception = true);
    
    
    /**
     * Return an address by its key. If it does not exist an exception will be 
     *  thrown or an empty string will be returned.
     * 
     * @param (int) $key
     * @param (string) $line_break character(s) to use as line break
     * @param (bool) $throw_exception
     * @return (string) 
     * @throws \Snap\Data\Exception\AddressDoesNotExistException
     */
    public function getAddress($key = 0, $line_break = '<br>', $throw_exception = true);
    
    
    /**
     * Return a address object by its description. If not found an exception 
     * will be thrown or null will be returned.
     * 
     * @param (string) $description
     * @param (bool) $throw_exception
     * @return \Snap\Data\Address|null
     * @throws \Snap\Data\Exception\AddressDoesNotExistException
     */
    public function getAddressObjectByDescription($description, $throw_exception = true);
    
    
    /**
     * Add a credit card.
     * 
     * @param string $card_number
     * @param string $expiration_month two digit numeric month
     * @param string $expiration_year two or four digit numeric year
     * @param string $verification_code card verification code (optional)
     * @param string $billing_postal_code billing postal code (optional)
     * @param string $description description (optional)
     * @param string $notes notes (optional)
     * @return \Snap\Data\Person
     */
    public function addCreditCard($card_number, $expiration_month, $expiration_year, 
            $verification_code = '', $billing_postal_code = '', $description = '', $notes = '');
    
    
    /**
     * Add a credit card object.
     * 
     * @param \Snap\Data\CreditCardInterface $CreditCard
     * @return \Snap\Data\Person
     */
    public function addCreditCardObject(CreditCardInterface $CreditCard);
    
    
    /**
     * Return an array of address objects.
     * 
     * @return array
     */
    public function getCreditCards();
    
    
    /**
     * Return a credit card object by its key. If it does not exist an exception 
     *  will be thrown or null will be returned.
     * 
     * @param (int) $key
     * @param (bool) $throw_exception
     * @return \Snap\Data\CreditCardInterface|null
     * @throws \Snap\Data\Exception\CardDoesNotExistException
     */
    public function getCreditCardObject($key = 0, $throw_exception = true);
        
    
    /**
     * Return a credit card object by its description. If not found an exception 
     * will be thrown or null will be returned.
     * 
     * @param (string) $description
     * @param (bool) $throw_exception
     * @return \Snap\Data\CreditCardInterface|null
     * @throws \Snap\Data\Exception\CardDoesNotExistException
     */
    public function getCreditCardObjectByDescription($description, $throw_exception = true);


    /**
     * Set the ID.
     * 
     * @param string $id
     * @return \Snap\Data\PersonInterface
     */
    public function setId($id);
    
    
    /**
     * Return the ID.
     * 
     * @return string
     */
    public function getId();

    
    /**
     * Set the description.
     * 
     * @param string $description
     * @return \Snap\Data\PersonInterface
     */
    public function setDescription($description);
    
    
    /**
     * Return the description.
     * 
     * @return string
     */
    public function getDescription();
    
    
    /**
     * Set the notes.
     * 
     * @param string $notes
     * @return \Snap\Data\PersonInterface
     */
    public function setNotes($notes);
    
    
    /**
     * Return the notes.
     * 
     * @return string
     */
    public function getNotes();
    
        
}

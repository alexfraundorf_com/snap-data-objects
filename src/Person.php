<?php
/**
 * The Snap person data class.  
 * 
 * @package \Snap\Data
 * @author Alex Fraundorf - AlexFraundorf.com
 * @copyright (c) 2018, Alex Fraundorf and Snap Programming and Development LLC
 * @version 1.0.0 03/27/2018
 * @since 1.0.0 03/14/2018
 * @license MIT License http://www.opensource.org/licenses/mit-license.php
 */
namespace Snap\Data;

require_once('DataAbstract.php');
require_once('PersonInterface.php');
use Snap\Data\Exception;

class Person extends DataAbstract implements PersonInterface {

    
    /**
     *
     * @var string first name
     */
    protected $first_name;
    
    /**
     *
     * @var string middle name
     */
    protected $middle_name;
    
    /**
     *
     * @var string last name
     */
    protected $last_name;
    
    /**
     *
     * @var string full name
     */
    protected $full_name;
    
    /**
     *
     * @var string suffix
     */
    protected $salutation;
    
    /**
     *
     * @var string salutation
     */
    protected $suffix;
    
    /**
     *
     * @var string title
     */
    protected $title;
    
    /**
     *
     * @var string organization name
     */
    protected $organization_name;
    
    /**
     *
     * @var string serial/social security number
     */
    protected $serial_number;
    
    /**
     *
     * @var array of \Snap\Data\Email objects
     */
    protected $emails = [];    
    
    /**
     *
     * @var array of \Snap\Data\Phone objects
     */
    protected $phones = [];
    
    /**
     *
     * @var array of \Snap\Data\Address objects
     */
    protected $addresses = [];
    
    /**
     *
     * @var array of \Snap\Data\CreditCard objects
     */
    protected $credit_cards = [];

    
    
    /**
     * Constructor
     * 
     * @param array $data optional array of data to set as object properties.
     * @param string $description (optional)
     * @param string $notes (optional)
     * @version 1.0.0 03/15/2018
     * @since 1.0.0 03/15/2018
     */
    public function __construct(array $data = [], $description = '', $notes = '') {
        if($data) {
            foreach($data as $name => $value) {
                $this->{$name} = $value;
            }
        }
        $this->setDescription($description);
        $this->setNotes($notes);
    }
    
    
    /**
     * Set the first name.
     * 
     * @param string $first_name
     * @return \Snap\Data\Person
     * @version 1.0.0 03/15/2018
     * @since 1.0.0 03/15/2018
     */
    public function setFirstName($first_name) {
        $this->first_name = (string) $first_name;
        return $this;
    }
    
    
    /**
     * Return the first name.
     * 
     * @return string
     * @version 1.0.0 03/15/2018
     * @since 1.0.0 03/15/2018
     */
    public function getFirstName() {
        if(isset($this->first_name)) {
            return (string) $this->first_name;
        }
        return '';
    }
    
    
    /**
     * Set the middle name.
     * 
     * @param string $middle_name
     * @return \Snap\Data\Person
     * @version 1.0.0 03/15/2018
     * @since 1.0.0 03/15/2018
     */
    public function setMiddleName($middle_name) {
        $this->middle_name = (string) $middle_name;
        return $this;
    }
    
    
    /**
     * Return the middle name.
     * 
     * @return string
     * @version 1.0.0 03/15/2018
     * @since 1.0.0 03/15/2018
     */
    public function getMiddleName() {
        if(isset($this->middle_name)) {
            return (string) $this->middle_name;
        }
        return '';
    }
    
    
    /**
     * Set the last name.
     * 
     * @param string $last_name
     * @return \Snap\Data\Person
     * @version 1.0.0 03/15/2018
     * @since 1.0.0 03/15/2018
     */
    public function setLastName($last_name) {
        $this->last_name = (string) $last_name;
        return $this;
    }
    
    
    /**
     * Return the last name.
     * 
     * @return string
     * @version 1.0.0 03/15/2018
     * @since 1.0.0 03/15/2018
     */
    public function getLastName() {
        if(isset($this->last_name)) {
            return (string) $this->last_name;
        }
        return '';
    }
    
    
    /**
     * Set the full name.
     * 
     * @param string $full_name
     * @return \Snap\Data\Person
     * @version 1.0.0 03/15/2018
     * @since 1.0.0 03/15/2018
     */
    public function setFullName($full_name) {
        $this->full_name = (string) $full_name;
        return $this;
    }
    
    
    /**
     * Build and set the full name from the set first, middle, last names and 
     *  suffix.
     * 
     * @return \Snap\Data\Person
     * @version 1.0.0 03/27/2018
     * @since 1.0.0 03/15/2018
     */
    public function buildFullName() {
        $full_name = '';
        if($this->getFirstName()) {
            $full_name .= $this->getFirstName() . ' ';
        }
        if($this->getMiddleName()) {
            $full_name .= $this->getMiddleName() . ' ';
        }
        if($this->getLastName()) {
            $full_name .= $this->getLastName();
        }
        if($this->getSuffix()) {
            $full_name .= ' ' . $this->getSuffix();
        }
        $this->setFullName(trim($full_name));
        return $this;
    }
    
    
    /**
     * Return the full name or an empty string if it has not been built.
     * 
     * @return string
     * @version 1.0.0 03/15/2018
     * @since 1.0.0 03/15/2018
     */
    public function getFullName() {
        if(isset($this->full_name)) {
            return (string) $this->full_name;
        }
        return '';
    }
    
    
    /**
     * Set the salutation.
     * 
     * @param string $salutation
     * @return \Snap\Data\Person
     * @version 1.0.0 03/15/2018
     * @since 1.0.0 03/15/2018
     */
    public function setSalutation($salutation) {
        $this->salutation = (string) $salutation;
        return $this;
    }
    
    
    /**
     * Return the salutation.
     * 
     * @return string
     * @version 1.0.0 03/15/2018
     * @since 1.0.0 03/15/2018
     */
    public function getSalutation() {
        if(isset($this->salutation)) {
            return (string) $this->salutation;
        }
        return '';
    }
    
    
    /**
     * Set the suffix.
     * 
     * @param string $suffix
     * @return \Snap\Data\Person
     * @version 1.0.0 03/27/2018
     * @since 1.0.0 03/27/2018
     */
    public function setSuffix($suffix) {
        $this->suffix = (string) $suffix;
        return $this;
    }
    
    
    /**
     * Return the suffix.
     * 
     * @return string
     * @version 1.0.0 03/27/2018
     * @since 1.0.0 03/27/2018
     */
    public function getSuffix() {
        if(isset($this->suffix)) {
            return (string) $this->suffix;
        }
        return '';
    }
    
    
    /**
     * Set the title.
     * 
     * @param string $title
     * @return \Snap\Data\Person
     * @version 1.0.0 03/15/2018
     * @since 1.0.0 03/15/2018
     */
    public function setTitle($title) {
        $this->title = (string) $title;
        return $this;
    }
    
    
    /**
     * Return the title.
     * 
     * @return string
     * @version 1.0.0 03/15/2018
     * @since 1.0.0 03/15/2018
     */
    public function getTitle() {
        if(isset($this->title)) {
            return (string) $this->title;
        }
        return '';
    }
    
    
    /**
     * Set the organization name.
     * 
     * @param string $name
     * @return \Snap\Data\Person
     * @version 1.0.0 03/15/2018
     * @since 1.0.0 03/15/2018
     */
    public function setOrganizationName($name) {
        $this->organization_name = (string) $name;
        return $this;
    }
    
    
    /**
     * Return the organization name.
     * 
     * @return string
     * @version 1.0.0 03/15/2018
     * @since 1.0.0 03/15/2018
     */
    public function getOrganizationName() {
        if(isset($this->organization_name)) {
            return (string) $this->organization_name;
        }
        return '';
    }
    
    
    /**
     * Set the serial/social security number.
     * 
     * Warning: If you are storing this data, make sure that you encrypt it!
     * 
     * @param string $serial_number
     * @return \Snap\Data\Person
     * @version 1.0.0 03/15/2018
     * @since 1.0.0 03/15/2018
     */
    public function setSerialNumber($serial_number) {
        $this->serial_number = (string) $serial_number;
        return $this;
    }
    
    
    /**
     * Return the serial/social security number.
     * 
     * Warning: If you are storing this data, make sure that you encrypt it!
     * 
     * @return string
     * @version 1.0.0 03/15/2018
     * @since 1.0.0 03/15/2018
     */
    public function getSerialNumber() {
        if(isset($this->serial_number)) {
            return (string) $this->serial_number;
        }
        return '';
    }
    
    
    /**
     * Add an email address.
     * 
     * @param string $email_address
     * @param string $description (optional)
     * @param string $notes (optional)
     * @return \Snap\Data\Person
     * @version 1.0.0 03/15/2018
     * @since 1.0.0 03/15/2018
     */
    public function addEmailAddress($email_address, $description = '', $notes = '') {
        $this->emails[] = new Email($email_address, $description, $notes);
        return $this;
    }
    
    
    /**
     * Add an email address object.
     * 
     * @param \Snap\Data\EmailInterface $Email
     * @return \Snap\Data\Person
     * @version 1.0.0 03/27/2018
     * @since 1.0.0 03/15/2018
     */
    public function addEmailAddressObject(EmailInterface $Email) {
        $this->emails[] = $Email;
        return $this;
    }
    
    
    /**
     * Return an array of email objects.
     * 
     * @return array
     * @version 1.0.0 03/15/2018
     * @since 1.0.0 03/15/2018
     */
    public function getEmailAddresses() {
        return (array) $this->emails;
    }
    
    
    /**
     * Return an email object by its key. If the key is not found, an exception 
     *  will be thrown or null will be returned.
     * 
     * @param (int) $key
     * @param (bool) $throw_exception
     * @return \Snap\Data\EmailInterface|null
     * @throws \Snap\Data\Exception\EmailDoesNotExistException
     * @version 1.0.0 03/15/2018
     * @since 1.0.0 03/15/2018
     */
    public function getEmailAddressObject($key = 0, $throw_exception = true) {
        if(isset($this->emails[(int) $key])) {
            return $this->emails[(int) $key];
        }
        if($throw_exception) {
            throw new Exception\EmailDoesNotExistException('No email address '
                    . 'exists at key: ' . (int) $key);
        }
        return null;
    } 
    
    
    /**
     * Return an email address by its key. If the key does not exist an exception 
     *  will be thrown or an empty string will be returned.
     * 
     * @param (int) $key
     * @param (bool) $throw_exception
     * @return (string) 
     * @throws \Snap\Data\Exception\EmailDoesNotExistException
     * @version 1.0.0 03/27/2018
     * @since 1.0.0 03/15/2018
     */
    public function getEmailAddress($key = 0, $throw_exception = true) {
        $Email = $this->getEmailAddressObject($key, false);
        if($Email) {
            return (string) $Email->getEmail();
        }
        if($throw_exception) {
            throw new Exception\EmailDoesNotExistException('No email address '
                    . 'exists at key: ' . (int) $key);
        }
        return '';
    }
    
    
    /**
     * Return an email object by its description. If not found, an exception 
     *  will be thrown or null will be returned.
     * 
     * @param (string) $description
     * @param (bool) $throw_exception
     * @return \Snap\Data\EmailInterface|null
     * @throws \Snap\Data\Exception\EmailDoesNotExistException
     * @version 1.0.0 03/27/2018
     * @since 1.0.0 03/15/2018
     */
    public function getEmailAddressObjectByDescription($description, $throw_exception = true) {
        foreach($this->emails as $Email) {
            if((string) $description === $Email->getDescription()) {
                return $Email;
            }
        }
        // description not found
        if($throw_exception) {
            throw new Exception\EmailDoesNotExistException('No email address '
                    . 'could be found with description: ' . (string) $description);
        }
        return null;
    }    
    
    
    /**
     * Add a phone number.
     * 
     * @param string $phone_number
     * @param string $description (optional)
     * @param string $notes (optional)
     * @return \Snap\Data\Person
     * @version 1.0.0 03/15/2018
     * @since 1.0.0 03/15/2018
     */
    public function addPhoneNumber($phone_number, $description = '', $notes = '') {
        $this->phones[] = new Phone($phone_number, $description, $notes);
        return $this;
    }
    
    
    /**
     * Add a phone number object.
     * 
     * @param \Snap\Data\PhoneInterface $Phone
     * @return \Snap\Data\Person
     * @version 1.0.0 03/27/2018
     * @since 1.0.0 03/15/2018
     */
    public function addPhoneNumberObject(PhoneInterface $Phone) {
        $this->phones[] = $Phone;
        return $this;
    }
    
    
    /**
     * Return an array of phone number objects.
     * 
     * @return array
     * @version 1.0.0 03/15/2018
     * @since 1.0.0 03/15/2018
     */
    public function getPhoneNumbers() {
        return (array) $this->phones;
    }
    
    
    /**
     * Return a phone number object by its key. If not found an exception will 
     *  be thrown or null will be returned.
     * 
     * @param (int) $key
     * @param (bool) $throw_exception
     * @return \Snap\Data\PhoneInterface|null
     * @throws \Snap\Data\Exception\PhoneDoesNotExistException
     * @version 1.0.0 03/27/2018
     * @since 1.0.0 03/15/2018
     */
    public function getPhoneNumberObject($key = 0, $throw_exception = true) {
        if(isset($this->phones[(int) $key])) {
            return $this->phones[(int) $key];
        }
        if($throw_exception) {
            throw new Exception\PhoneDoesNotExistException('No phone number '
                    . 'exists at key: ' . (int) $key);
        }
        return null;
    } 
    
    
    /**
     * Return a phone number by its key. If not found an exception will be 
     *  thrown or an empty string will be returned.
     * 
     * @param (int) $key
     * @param (bool) $throw_exception
     * @return (string) 
     * @throws \Snap\Data\Exception\PhoneDoesNotExistException
     * @version 1.0.0 03/27/2018
     * @since 1.0.0 03/15/2018
     */
    public function getPhoneNumber($key = 0, $throw_exception = true) {
        $Phone = $this->getPhoneNumberObject($key, false);
        if($Phone) {
            return (string) $Phone->getPhoneNumber();
        }
        if($throw_exception) {
            throw new Exception\PhoneDoesNotExistException('No phone number '
                    . 'exists at key: ' . (int) $key);
        }
        return '';
    }
    
    
    /**
     * Return a phone number object by its description. If not found an exception 
     *  will be thrown or null will be returned.
     * 
     * @param (string) $description
     * @param (bool) $throw_exception
     * @return \Snap\Data\PhoneInterface|null
     * @throws \Snap\Data\Exception\PhoneDoesNotExistException
     * @version 1.0.0 03/27/2018
     * @since 1.0.0 03/15/2018
     */
    public function getPhoneNumberObjectByDescription($description, $throw_exception = true) {
        foreach($this->phones as $Phone) {
            if((string) $description === $Phone->getDescription()) {
                return $Phone;
            }
        }
        // description not found
        if($throw_exception) {
            throw new Exception\PhoneDoesNotExistException('No phone number '
                    . 'could be found with description: ' . (string) $description);
        }
        return null;
    }    
    
    
    /**
     * Add an address.
     * 
     * @param string $address1 address line 1
     * @param string $city city
     * @param string $state_province state or province
     * @param string $postal_code postal code
     * @param string $description (optional)
     * @param string $notes (optional)
     * @return \Snap\Data\Person
     * @version 1.0.0 03/27/2018
     * @since 1.0.0 03/15/2018
     */
    public function addAddress($address1, $city, $state_province, $postal_code, 
            $description = '', $notes = '') {
        $Address = new Address($address1, $city, $state_province, $postal_code, $description, $notes);
        $this->addresses[] = $Address;
        return $this;
    }
    
    
    /**
     * Add an address object.
     * 
     * @param \Snap\Data\AddressInterface $Address
     * @return \Snap\Data\Person
     * @version 1.0.0 03/27/2018
     * @since 1.0.0 03/15/2018
     */
    public function addAddressObject(AddressInterface $Address) {
        $this->addresses[] = $Address;
        return $this;
    }
    
    
    /**
     * Return an array of address objects.
     * 
     * @return array
     * @version 1.0.0 03/15/2018
     * @since 1.0.0 03/15/2018
     */
    public function getAddresses() {
        return (array) $this->addresses;
    }
    
    
    /**
     * Return an address object by its key. If it does not exist an exception 
     *  will be thrown or null will be returned.
     * 
     * @param (int) $key
     * @param (bool) $throw_exception
     * @return \Snap\Data\AddressInterface|null
     * @throws \Snap\Data\Exception\AddressDoesNotExistException
     * @version 1.0.0 03/27/2018
     * @since 1.0.0 03/15/2018
     */
    public function getAddressObject($key = 0, $throw_exception = true) {
        if(isset($this->addresses[(int) $key])) {
            return $this->addresses[(int) $key];
        }
        if($throw_exception) {
            throw new Exception\AddressDoesNotExistException('No address exists '
                    . 'at key: ' . (int) $key);
        }
        return null;
    } 
    
    
    /**
     * Return an address by its key. If it does not exist an exception will be 
     *  thrown or an empty string will be returned.
     * 
     * @param (int) $key
     * @param (string) $line_break character(s) to use as line break
     * @param (bool) $throw_exception
     * @return (string) 
     * @throws \Snap\Data\Exception\AddressDoesNotExistException
     * @version 1.0.0 03/27/2018
     * @since 1.0.0 03/15/2018
     */
    public function getAddress($key = 0, $line_break = '<br>', $throw_exception = true) {
        $Address = $this->getAddressObject($key, false);
        if($Address) {
            return (string) $Address->toString($line_break);
        }
        if($throw_exception) {
            throw new Exception\AddressDoesNotExistException('No address exists '
                    . 'at key: ' . (int) $key);
        }
        return '';
    }
    
    
    /**
     * Return a address object by its description. If not found an exception 
     * will be thrown or null will be returned.
     * 
     * @param (string) $description
     * @param (bool) $throw_exception
     * @return \Snap\Data\AddressInterface|null
     * @throws \Snap\Data\Exception\AddressDoesNotExistException
     * @version 1.0.0 03/27/2018
     * @since 1.0.0 03/15/2018
     */
    public function getAddressObjectByDescription($description, $throw_exception = true) {
        foreach($this->addresses as $Address) {
            if((string) $description === $Address->getDescription()) {
                return $Address;
            }
        }
        // description not found
        if($throw_exception) {
            throw new Exception\AddressDoesNotExistException('No address could '
                    . 'be found with description: ' . (string) $description);
        }
        return null;
    }    
    
    
    /**
     * Add a credit card.
     * 
     * @param string $card_number
     * @param string $expiration_month two digit numeric month
     * @param string $expiration_year two or four digit numeric year
     * @param string $verification_code card verification code (optional)
     * @param string $billing_postal_code billing postal code (optional)
     * @param string $description description (optional)
     * @param string $notes notes (optional)
     * @return \Snap\Data\Person
     * @version 1.0.0 03/27/2018
     * @since 1.0.0 03/27/2018
     */
    public function addCreditCard($card_number, $expiration_month, $expiration_year, 
            $verification_code = '', $billing_postal_code = '', $description = '', $notes = '') {
        $CC = new CreditCard($card_number, $expiration_month, $expiration_year, 
                $verification_code, $billing_postal_code, $description, $notes);
        $this->credit_cards[] = $CC;
        return $this;
    }
    
    
    /**
     * Add a credit card object.
     * 
     * @param \Snap\Data\CreditCardInterface $CreditCard
     * @return \Snap\Data\Person
     * @version 1.0.0 03/27/2018
     * @since 1.0.0 03/27/2018
     */
    public function addCreditCardObject(CreditCardInterface $CreditCard) {
        $this->credit_cards[] = $CreditCard;
        return $this;
    }
    
    
    /**
     * Return an array of address objects.
     * 
     * @return array
     * @version 1.0.0 03/27/2018
     * @since 1.0.0 03/27/2018
     */
    public function getCreditCards() {
        return (array) $this->credit_cards;
    }
    
    
    /**
     * Return a credit card object by its key. If it does not exist an exception 
     *  will be thrown or null will be returned.
     * 
     * @param (int) $key
     * @param (bool) $throw_exception
     * @return \Snap\Data\CreditCardInterface|null
     * @throws \Snap\Data\Exception\CardDoesNotExistException
     * @version 1.0.0 03/27/2018
     * @since 1.0.0 03/27/2018
     */
    public function getCreditCardObject($key = 0, $throw_exception = true) {
        if(isset($this->credit_cards[(int) $key])) {
            return $this->credit_cards[(int) $key];
        }
        if($throw_exception) {
            throw new Exception\CardDoesNotExistException('No credit card '
                    . 'exists at key: ' . (int) $key);
        }
        return null;
    } 
        
    
    /**
     * Return a credit card object by its description. If not found an exception 
     * will be thrown or null will be returned.
     * 
     * @param (string) $description
     * @param (bool) $throw_exception
     * @return \Snap\Data\CreditCardInterface|null
     * @throws \Snap\Data\Exception\CardDoesNotExistException
     * @version 1.0.0 03/27/2018
     * @since 1.0.0 03/27/2018
     */
    public function getCreditCardObjectByDescription($description, $throw_exception = true) {
        foreach($this->credit_cards as $CreditCard) {
            if((string) $description === $CreditCard->getDescription()) {
                return $CreditCard;
            }
        }
        // description not found
        if($throw_exception) {
            throw new Exception\CardDoesNotExistException('No credit card could '
                    . 'be found with description: ' . (string) $description);
        }
        return null;
    }    
    
    
}

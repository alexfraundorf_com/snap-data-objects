<?php
/**
 * Factory methods for Snap data classes.
 * 
 * @package \Snap\Data
 * @author Alex Fraundorf - AlexFraundorf.com
 * @copyright (c) 2018, Alex Fraundorf and Snap Programming and Development LLC
 * @version 1.0.0 03/15/2018
 * @since 1.0.0 03/15/2018
 * @license MIT License http://www.opensource.org/licenses/mit-license.php
 */
namespace Snap\Data;

class Factory {
 

    /**
     * Return a new Address object.
     * 
     * @param string $address1 address line 1
     * @param string $city city
     * @param string $state_province state or province
     * @param string $postal_code postal code
     * @param string $description (optional)
     * @param string $notes (optional)
     * @return \Snap\Data\Address
     * @version 1.0.0 03/15/2018
     * @since 1.0.0 03/15/2018
     */
    public static function addressFactory($address1, $city, $state_province, 
            $postal_code, $description = '', $notes = '') {
        return new Address($address1, $city, $state_province, $postal_code, 
                $description, $notes);
    }
    
    
    /**
     * Return a new CreditCard object.
     * 
     * @param string $card_number
     * @param string $expiration_month two digit numeric month
     * @param string $expiration_year two or four digit numeric year
     * @param string $verification_code card verification code (optional)
     * @param string $billing_postal_code billing postal code (optional)
     * @param string $description description (optional)
     * @param string $notes notes (optional)
     * @return \Snap\Data\CreditCard
     * @version 1.0.0 03/27/2018
     * @since 1.0.0 03/15/2018
     */
    public static function creditCardFactory($card_number, $expiration_month, 
            $expiration_year, $verification_code, $billing_postal_code, 
            $description = '', $notes = '') {
        return new CreditCard($card_number, $expiration_month, $expiration_year, 
                $verification_code, $billing_postal_code, $description, $notes);
    }
    
    
    /**
     * Return a new Email object.
     * 
     * @param string $email_address
     * @param string $description (optional)
     * @param string $notes (optional)
     * @return \Snap\Data\Email
     * @version 1.0.0 03/15/2018
     * @since 1.0.0 03/14/2018
     */
    public static function emailFactory($email_address, $description = '', $notes = '') {
        return new Email($email_address, $description, $notes);
    }
    
    
    /**
     * Return a new Phone object.
     * 
     * @param array $data optional array of data to set as object properties.
     * @param string $description (optional)
     * @param string $notes (optional)
     * @version 1.0.0 03/27/2018
     * @since 1.0.0 03/27/2018
     */
    public static function personFactory(array $data = [], $description = '', $notes = '') {
        return new Person($data, $description, $notes);
    }
    
    
    /**
     * Return a new Phone object.
     * 
     * Note: Setting a phone number with an extension will result in a failed 
     *  validation.
     * 
     * @param string $phone_number
     * @param string $description (optional)
     * @param string $notes (optional)
     * @param bool $validate_and_format_as_us_phone_number 
     * @version 1.0.0 03/27/2018
     * @since 1.0.0 03/27/2018
     */
    public static function phoneFactory($phone_number, $description = '', $notes = '', 
            $validate_and_format_as_us_phone_number = true) {
        return new Phone($phone_number, $description, $notes, $validate_and_format_as_us_phone_number);
    }
    
    
}

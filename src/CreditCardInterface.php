<?php
/**
 * Interface for the credit card data class.  
 * 
 * @package \Snap\Data
 * @author Alex Fraundorf - AlexFraundorf.com
 * @copyright (c) 2018, Alex Fraundorf and Snap Programming and Development LLC
 * @version 1.0.0 03/27/2018
 * @since 1.0.0 03/27/2018
 * @license MIT License http://www.opensource.org/licenses/mit-license.php
 */
namespace Snap\Data;

interface CreditCardInterface {
 

    /**
     * Constructor
     * 
     * @param string $card_number
     * @param string $expiration_month two digit numeric month
     * @param string $expiration_year two or four digit numeric year
     * @param string $verification_code card verification code (optional)
     * @param string $billing_postal_code billing postal code (optional)
     * @param string $description description (optional)
     * @param string $notes notes (optional)
     */
    public function __construct($card_number, $expiration_month, $expiration_year, 
            $verification_code = '', $billing_postal_code = '', $description = '', $notes = '');

    
    /**
     * Validate, format and set the card number.
     * 
     * @param string $card_number
     * @param bool $validate if true (default) the card number will be validated
     * @return \Snap\Data\CreditCard
     * @throws \Snap\Data\Exception\InvalidCardNumberException
     */
    public function setNumber($card_number, $validate = true);
    
    
    /**
     * Return the card number.
     * 
     * @return string
     */
    public function getNumber();


    /**
     * Validate, format and set the expiration month.
     * 
     * @param string $month two digit numeric month.
     * @return \Snap\Data\CreditCard
     * @throws \InvalidArgumentException
     */
    public function setExpirationMonth($month);
    
    
    /**
     * Return the expiration month.
     * 
     * @return string
     */
    public function getExpirationMonth();

    
    /**
     * Validate, format and set the expiration year.
     * 
     * @param string $year two or four digit numeric year.
     * @return \Snap\Data\CreditCard
     * @throws \Snap\Data\Exception\ExpiredCardException
     * @throws \OutOfRangeException
     */
    public function setExpirationYear($year);
    
    
    /**
     * Return the expiration year.
     * 
     * @return string
     */
    public function getExpirationYear();

    
    /**
     * Validate and set the verification_code.
     * 
     * @param string $code 3 or 4 digit numeric code
     * @return \Snap\Data\CreditCard
     * @throws \InvalidArgumentException
     */
    public function setVerificationCode($code);
    
    
    /**
     * Return the verification code.
     * 
     * @return string
     */
    public function getVerificationCode();

    
    /**
     * Set the billing postal code.
     * 
     * @param string $postal_code
     * @return \Snap\Data\CreditCard
     */
    public function setPostalCode($postal_code);
    
    
    /**
     * Return the billing postal code.
     * 
     * @return string
     */
    public function getPostalCode();

    
    /**
     * Set the billing address object and the postal code.
     * 
     * @param \Snap\Data\Address $BillingAddress
     * @return \Snap\Data\CreditCardInterface
     */
    public function setBillingAddressObject(Address $BillingAddress);
    
    
    /**
     * Return the billing address object or null if there is none.
     * 
     * @return \Snap\Data\Address|null
     */
    public function getBillingAddressObject();
    
    
    /**
     * Validate that the minimum card data is set and optionally throw an 
     *  exception if it is not.
     * 
     * @param bool $throw_exception
     * @return boolean
     * @throws \Snap\Data\Exception\InvalidCardDataObjectException
     */
    public function validateMinimumDataSet($throw_exception = true);
    
    
    /**
     * Validate a card number according to the Luhn algorithm.
     * 
     * @param string $card_number The card number to validate
     * @param bool throw exception
     * @return boolean of card number validation
     * @throws \Snap\Data\Exception\InvalidCardNumberException
     */
    public static function validateCardNumber($card_number, $throw_exception = true);
    
    
    /**
     * Validate a provided expiration month and year and optionally throw an 
     *  exception on failure.
     * 
     * @param string $month two digit numeric month
     * @param string $year two or four digit numeric year
     * @param bool $throw_exception 
     * @return boolean
     * @throws \InvalidArgumentException
     * @throws \Snap\Data\Exception\ExpiredCardException
     * @throws \OutOfRangeException
     */
    public static function validateExpirationDate($month, $year, $throw_exception = true);


    /**
     * Set the ID.
     * 
     * @param string $id
     * @return \Snap\Data\CreditCardInterface
     */
    public function setId($id);
    
    
    /**
     * Return the ID.
     * 
     * @return string
     */
    public function getId();

    
    /**
     * Set the description.
     * 
     * @param string $description
     * @return \Snap\Data\CreditCardInterface
     */
    public function setDescription($description);
    
    
    /**
     * Return the description.
     * 
     * @return string
     */
    public function getDescription();
    
    
    /**
     * Set the notes.
     * 
     * @param string $notes
     * @return \Snap\Data\CreditCardInterface
     */
    public function setNotes($notes);
    
    
    /**
     * Return the notes.
     * 
     * @return string
     */
    public function getNotes();
    
        
}

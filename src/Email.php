<?php
/**
 * The Snap email data class.  
 * 
 * @package \Snap\Data
 * @author Alex Fraundorf - AlexFraundorf.com
 * @copyright (c) 2018, Alex Fraundorf and Snap Programming and Development LLC
 * @version 1.0.0 03/27/2018
 * @since 1.0.0 03/14/2018
 * @license MIT License http://www.opensource.org/licenses/mit-license.php
 */
namespace Snap\Data;

require_once('DataAbstract.php');
require_once('EmailInterface.php');
use Snap\Data\Exception;

class Email extends DataAbstract implements EmailInterface {
 
    /**
     *
     * @var string email address
     */
    protected $email_address;
    

    /**
     * Constructor.
     * 
     * @param string $email_address
     * @param string $description (optional)
     * @param string $notes (optional)
     * @version 1.0.0 03/15/2018
     * @since 1.0.0 03/14/2018
     */
    public function __construct($email_address, $description = '', $notes = '') {
        $this->setEmail($email_address);
        $this->description = (string) $description;
        $this->notes = (string) $notes;
    }


    /**
     * Validate and set the email address.
     * 
     * @param string $email
     * @return \Snap\Data\Email
     * @version 1.0.0 03/14/2018
     * @since 1.0.0 03/14/2018
     */
    public function setEmail($email) {
        $this->validateEmail($email);
        $this->email_address = (string) $email;
        return $this;
    }
    
    
    /**
     * Return the email address.
     * 
     * @return string
     * @version 1.0.0 03/14/2018
     * @since 1.0.0 03/14/2018
     */
    public function getEmail() {
        return (string) $this->email_address;
    }

    
    /**
     * Magic to string method.
     * 
     * @return string the email address
     * @version 1.0.0 03/15/2018
     * @since 1.0.0 03/15/2018
     */
    public function __toString() {
        return (string) $this->getEmail();
    }
    
    
    /**
     * Validate an email address and optionally throw an exception on failure.
     * 
     * @param (string) $email the email address to validate
     * @param (bool) $throw_exception if true an exception will be thrown on 
     *  failed validation
     * @return (bool) of validation
     * @throws \Snap\Data\Exception\InvalidEmailException
     * @version 1.0.0 03/27/2018
     * @since 1.0.0 03/14/2018
     */
    public static function validateEmail($email, $throw_exception = true) {
        if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            if($throw_exception) {
                throw new Exception\InvalidEmailException('Email address ('
                        . $email . ') is invalid.');
            }
            return false;
        }  
        return true;
    }
    
    
}

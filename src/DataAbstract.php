<?php
/**
 * Abstract class providing common functionality.  
 * 
 * @package \Snap\Data
 * @author Alex Fraundorf - AlexFraundorf.com
 * @copyright (c) 2018, Alex Fraundorf and Snap Programming and Development LLC
 * @version 1.0.0 03/27/2018
 * @since 1.0.0 03/15/2018
 * @license MIT License http://www.opensource.org/licenses/mit-license.php
 */
namespace Snap\Data;

abstract class DataAbstract {

    
    /**
     *
     * @var string person id
     */
    protected $id;
    
    /**
     *
     * @var string description ie: personal, work, etc.
     */
    protected $description;
    
    /**
     *
     * @var string description ie: personal, work, etc.
     */
    protected $notes;
    


    /**
     * Set the ID.
     * 
     * @param string $id
     * @return \Snap\Data\DataAbstract
     * @version 1.0.0 03/15/2018
     * @since 1.0.0 03/15/2018
     */
    public function setId($id) {
        $this->id = (string) $id;
        return $this;
    }
    
    
    /**
     * Return the ID.
     * 
     * @return string
     * @version 1.0.0 03/15/2018
     * @since 1.0.0 03/15/2018
     */
    public function getId() {
        if(isset($this->id)) {
            return (string) $this->id;
        }
        return '';
    }

    
    /**
     * Set the description.
     * 
     * @param string $description
     * @return \Snap\Data\DataAbstract
     * @version 1.0.0 03/14/2018
     * @since 1.0.0 03/14/2018
     */
    public function setDescription($description) {
        $this->description = (string) $description;
        return $this;
    }
    
    
    /**
     * Return the description.
     * 
     * @return string
     * @version 1.0.0 03/14/2018
     * @since 1.0.0 03/14/2018
     */
    public function getDescription() {
        if(isset($this->description)) {
            return (string) $this->description;
        }
        return '';
    }
    
    
    /**
     * Set the notes.
     * 
     * @param string $notes
     * @return \Snap\Data\DataAbstract
     * @version 1.0.0 03/15/2018
     * @since 1.0.0 03/15/2018
     */
    public function setNotes($notes) {
        $this->notes = (string) $notes;
        return $this;
    }
    
    
    /**
     * Return the notes.
     * 
     * @return string
     * @version 1.0.0 03/15/2018
     * @since 1.0.0 03/15/2018
     */
    public function getNotes() {
        if(isset($this->notes)) {
            return (string) $this->notes;
        }
        return '';
    }
    
    
}

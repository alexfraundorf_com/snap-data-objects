<?php
/**
 * Invalid address data object exception. 
 * 
 * @package \Snap\Data
 * @author Alex Fraundorf - AlexFraundorf.com
 * @copyright (c) 2018, Alex Fraundorf and Snap Programming and Development LLC
 * @version 1.0.0 03/22/2018
 * @since 1.0.0 03/22/2018
 * @license MIT License http://www.opensource.org/licenses/mit-license.php
 */
namespace Snap\Data\Exception;


class InvalidAddressDataObjectException extends \ErrorException {
    
    
    /**
     *
     * @var array of exception error codes and their meanings
     */
    protected $error_codes = [
        1 => 'Street address 1 must have a value.',
        2 => 'City must have a value.',
        3 => 'State/province must have a value.',
        4 => 'Postal code must have a value.',
    ];
    
    
    
    /**
     * Exception constructor
     * 
     * @param string $message exception message
     * @param int $code exception code (optional)
     * @param \Throwable $previous previously caught exception (optional)
     * @version 1.0.0 03/22/2018
     * @since 1.0.0 03/22/2018
     */
    public function __construct(string $message = "", int $code = 0, \Throwable $previous = null) {
        parent::__construct($message, $code, $previous);
    }

    
    /**
     * Return the meaning of an error code.
     * 
     * @param int $error_code
     * @return string
     * @throws \UnexpectedValueException if the error code is not set
     * @version 1.0.0 03/22/2018
     * @since 1.0.0 03/22/2018
     */
    public function getErrorCodeMeaning($error_code) {
        if(isset($this->error_codes[(int) $error_code])) {
            return (string) $this->error_codes[(int) $error_code];
        }
        throw new \UnexpectedValueException('There is no error code assigned to: ' . $error_code);
    }
    
}

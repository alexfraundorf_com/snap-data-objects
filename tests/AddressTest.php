<?php

namespace Snap\Data;

/**
 * Generated by PHPUnit_SkeletonGenerator on 2018-03-15 at 14:11:41.
 */
class AddressTest extends \PHPUnit_Framework_TestCase {

    /**
     * @var Address
     */
    protected $Address;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp() {
        require_once('../src/DataAbstract.php');
        require_once('../src/AddressInterface.php');
        require_once('../src/Address.php');
        require_once('dummy_objects/AddressDummy.php');
        $this->Address = new AddressDummy('123 Fake Street', 'Appleton', 'WI', '54914', 'work', 'This is my work address');
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown() {
        
    }

    /**
     * @covers Snap\Data\Address::__construct
     */
    public function testConstructor() {
        $Address = new Address('123 Fake St.', 'Appleton', 'WI', '54915', 'description', 'notes');
        $this->assertSame($Address->getAddress1(), '123 Fake St.');
        $this->assertSame($Address->getCity(), 'Appleton');
        $this->assertSame($Address->getStateProvince(), 'WI');
        $this->assertSame($Address->getPostalCode(), '54915');
        $this->assertSame($Address->getDescription(), 'description');
        $this->assertSame($Address->getNotes(), 'notes');
    }

    /**
     * @covers Snap\Data\Address::setAddress1
     */
    public function testSetAddress1() {
        $this->Address->setAddress1('321 Fake Street');
        $this->assertSame($this->Address->getAddress1(), '321 Fake Street');
    }

    /*
     * @covers Snap\Data\Address::getAddress1
     */
    public function testGetAddress1() {
        $this->assertSame($this->Address->getAddress1(), '123 Fake Street');
        $this->Address->unsetAddress1();
        $this->assertSame($this->Address->getAddress1(), '');
    }

    /**
     * @covers Snap\Data\Address::setAddress2
     */
    public function testSetAddress2() {
        $this->Address->setAddress2('Apartment B4 - Front Door');
        $this->assertSame($this->Address->getAddress2(), 'Apartment B4 - Front Door');
    }

    /**
     * @covers Snap\Data\Address::getAddress2
     */
    public function testGetAddress2() {
        $this->assertSame($this->Address->getAddress2(), '');
        $this->Address->setAddress2('Apartment B4 - Front Door');
        $this->assertSame($this->Address->getAddress2(), 'Apartment B4 - Front Door');
    }

    /**
     * @covers Snap\Data\Address::setAddress3
     */
    public function testSetAddress3() {
        $this->Address->setAddress3('P.O. Box 123456');
        $this->assertSame($this->Address->getAddress3(), 'P.O. Box 123456');
    }

    /**
     * @covers Snap\Data\Address::getAddress3
     */
    public function testGetAddress3() {
        $this->assertSame($this->Address->getAddress3(), '');
        $this->Address->setAddress3('P.O. Box 123456');
        $this->assertSame($this->Address->getAddress3(), 'P.O. Box 123456');
    }

    /**
     * @covers Snap\Data\Address::setCity
     */
    public function testSetCity() {
        $this->Address->setCity('Springfield');
        $this->assertSame($this->Address->getCity(), 'Springfield');
        $this->Address->unsetCity();
        $this->assertSame($this->Address->getCity(), '');
    }

    /**
     * @covers Snap\Data\Address::getCity
     */
    public function testGetCity() {
        $this->assertSame($this->Address->getCity(), 'Appleton');
        $this->Address->unsetCity();
        $this->assertSame($this->Address->getCity(), '');
    }

    /**
     * @covers Snap\Data\Address::setStateProvince
     */
    public function testSetStateProvince() {
        $this->Address->setStateProvince('IL');
        $this->assertSame($this->Address->getStateProvince(), 'IL');
    }

    /**
     * @covers Snap\Data\Address::getStateProvince
     */
    public function testGetStateProvince() {
        $this->assertSame($this->Address->getStateProvince(), 'WI');
        $this->Address->unsetStateProvince();
        $this->assertSame($this->Address->getStateProvince(), '');
    }

    /**
     * @covers Snap\Data\Address::setPostalCode
     */
    public function testSetPostalCode() {
        $this->Address->setPostalCode('54545-1234');
        $this->assertSame($this->Address->getPostalCode(), '54545-1234');
        $this->Address->setPostalCode('54545');
        $this->assertSame($this->Address->getPostalCode(), '54545');
        $this->Address->setPostalCode(54545);
        $this->assertSame($this->Address->getPostalCode(), '54545');
    }

    /**
     * @covers Snap\Data\Address::getPostalCode
     */
    public function testGetPostalCode() {
        $this->assertSame($this->Address->getPostalCode(), '54914');
        $this->Address->unsetPostalCode();
        $this->assertSame($this->Address->getPostalCode(), '');
    }

    /**
     * @covers Snap\Data\Address::setRegion
     */
    public function testSetRegion() {
        $this->Address->setRegion('Cook County');
        $this->assertSame($this->Address->getRegion(), 'Cook County');
    }

    /**
     * @covers Snap\Data\Address::getRegion
     */
    public function testGetRegion() {
        $this->assertSame($this->Address->getRegion(), '');
        $this->Address->setRegion('Cook County');
        $this->assertSame($this->Address->getRegion(), 'Cook County');
    }

    /**
     * @covers Snap\Data\Address::setCountry
     */
    public function testSetCountry() {
        $this->Address->setCountry('U.S.A.');
        $this->assertSame($this->Address->getCountry(), 'U.S.A.');
    }

    /**
     * @covers Snap\Data\Address::getCountry
     */
    public function testGetCountry() {
        $this->assertSame($this->Address->getCountry(), '');
        $this->Address->setCountry('U.S.A.');
        $this->assertSame($this->Address->getCountry(), 'U.S.A.');
    }

    /**
     * @covers Snap\Data\Address::toString
     */
    public function testToString() {
        $Address = new Address('123 Fake Street', 'Appleton', 'WI', '54915');
        $this->assertSame($Address->toString(), '123 Fake Street<br>Appleton, WI 54915');
        $Address2 = new Address('555 Fake Street', 'Appleton', 'WI', '54915');
        $Address2
                ->setAddress2('Suite 1001')
                ->setAddress3('Third Floor')
                ->setRegion('Cook County')
                ->setCountry('USA')
                ;
        $this->assertSame($Address2->toString(), '555 Fake Street<br>Suite 1001<br>Third Floor<br>Appleton, WI 54915<br>Cook County<br>USA');
    }

    /**
     * @covers Snap\Data\Address::__toString
     */
    public function test__toString() {
        $Address = new Address('123 Fake Street', 'Appleton', 'WI', '54915');
        $this->assertSame((string) $Address, '123 Fake Street<br>Appleton, WI 54915');
        $Address2 = new Address('555 Fake Street', 'Appleton', 'WI', '54915');
        $Address2
                ->setAddress2('Suite 1001')
                ->setAddress3('Third Floor')
                ->setRegion('Cook County')
                ->setCountry('USA')
                ;
        $this->assertSame((string) $Address2, '555 Fake Street<br>Suite 1001<br>Third Floor<br>Appleton, WI 54915<br>Cook County<br>USA');
    }

    /**
     * @covers Snap\Data\Address::validateMinimumAddress
     */
    public function testValidateMinimumAddress() {
        $this->assertTrue($this->Address->validateMinimumAddress());
        
        $this->Address->unsetAddress1();
        $this->assertFalse($this->Address->validateMinimumAddress(false));
        try {
            $this->Address->validateMinimumAddress();
            $this->assertError((__LINE__ - 1) . ' should have thrown an exception.');
        } catch (\Exception $ex) {
            $this->assertTrue($ex instanceof \Snap\Data\Exception\InvalidAddressDataObjectException);
            $this->assertSame($ex->getCode(), 1);
        }
        
        $this->Address->setAddress1('123 Fake Street')->unsetCity();
        $this->assertFalse($this->Address->validateMinimumAddress(false));
        try {
            $this->Address->validateMinimumAddress();
            $this->assertError((__LINE__ - 1) . ' should have thrown an exception.');
        } catch (\Exception $ex) {
            $this->assertTrue($ex instanceof \Snap\Data\Exception\InvalidAddressDataObjectException);
            $this->assertSame($ex->getCode(), 2);
        }
        
        $this->Address->setCity('Appleton')->unsetStateProvince();
        $this->assertFalse($this->Address->validateMinimumAddress(false));
        try {
            $this->Address->validateMinimumAddress();
            $this->assertError((__LINE__ - 1) . ' should have thrown an exception.');
        } catch (\Exception $ex) {
            $this->assertTrue($ex instanceof \Snap\Data\Exception\InvalidAddressDataObjectException);
            $this->assertSame($ex->getCode(), 3);
        }
        
        $this->Address->setStateProvince('WI')->unsetPostalCode();
        $this->assertFalse($this->Address->validateMinimumAddress(false));
        try {
            $this->Address->validateMinimumAddress();
            $this->assertError((__LINE__ - 1) . ' should have thrown an exception.');
        } catch (\Exception $ex) {
            $this->assertTrue($ex instanceof \Snap\Data\Exception\InvalidAddressDataObjectException);
            $this->assertSame($ex->getCode(), 4);
        }
    }

    /**
     * @covers Snap\Data\Address::validateZipCode
     */
    public function testValidateZipCode() {
        $this->assertTrue(Address::validateZipCode('90210'));
        $this->assertTrue(Address::validateZipCode(90210));
        $this->assertTrue(Address::validateZipCode('90210-1234'));
        $this->assertTrue(Address::validateZipCode('902101234'));
        $this->assertFalse(Address::validateZipCode('90210-12', false));
        $this->assertFalse(Address::validateZipCode('9021', false));
        $this->assertFalse(Address::validateZipCode('902102', false));
        try {
            Address::validateZipCode('90210-12');
            $this->assertError((__LINE__ - 1) . ' should have thrown an exception.');
        } catch (\Exception $ex) {
            $this->assertTrue($ex instanceof \Snap\Data\Exception\InvalidPostalCodeException);
        }
        try {
            Address::validateZipCode('9021');
            $this->assertError((__LINE__ - 1) . ' should have thrown an exception.');
        } catch (\Exception $ex) {
            $this->assertTrue($ex instanceof \Snap\Data\Exception\InvalidPostalCodeException);
        }
        try {
            Address::validateZipCode('902102');
            $this->assertError((__LINE__ - 1) . ' should have thrown an exception.');
        } catch (\Exception $ex) {
            $this->assertTrue($ex instanceof \Snap\Data\Exception\InvalidPostalCodeException);
        }
    }

    /**
     * @covers Snap\Data\Address::validateCanadianPostalCode
     */
    public function testValidateCanadianPostalCode() {
        $this->assertTrue(Address::validateCanadianPostalCode('M4A 1K9'));
        $this->assertTrue(Address::validateCanadianPostalCode('M4A1K9'));
        $this->assertFalse(Address::validateCanadianPostalCode('M4A-1K9', false));
        $this->assertFalse(Address::validateCanadianPostalCode('M4A 1K95', false));
        $this->assertFalse(Address::validateCanadianPostalCode('M4A 1K', false));
        $this->assertFalse(Address::validateCanadianPostalCode('M4A 19K', false));
        try {
            Address::validateCanadianPostalCode('M4A-1K9');
            $this->assertError((__LINE__ - 1) . ' should have thrown an exception.');
        } catch (\Exception $ex) {
            $this->assertTrue($ex instanceof \Snap\Data\Exception\InvalidPostalCodeException);
        }
        try {
            Address::validateCanadianPostalCode('M4A 1K95');
            $this->assertError((__LINE__ - 1) . ' should have thrown an exception.');
        } catch (\Exception $ex) {
            $this->assertTrue($ex instanceof \Snap\Data\Exception\InvalidPostalCodeException);
        }
        try {
            Address::validateCanadianPostalCode('M4A-1K');
            $this->assertError((__LINE__ - 1) . ' should have thrown an exception.');
        } catch (\Exception $ex) {
            $this->assertTrue($ex instanceof \Snap\Data\Exception\InvalidPostalCodeException);
        }
        try {
            Address::validateCanadianPostalCode('M4A 19K');
            $this->assertError((__LINE__ - 1) . ' should have thrown an exception.');
        } catch (\Exception $ex) {
            $this->assertTrue($ex instanceof \Snap\Data\Exception\InvalidPostalCodeException);
        }
    }

    /**
     * @covers Snap\Data\Address::validateGreatBritainPostalCode
     */
    public function testValidateGreatBritainPostalCode() {
        $this->assertTrue(Address::validateGreatBritainPostalCode('SW1A 1AA'));
        $this->assertTrue(Address::validateGreatBritainPostalCode('SW1A1AA'));
        $this->assertFalse(Address::validateGreatBritainPostalCode('SW1A 1A', false));
        $this->assertFalse(Address::validateGreatBritainPostalCode('SW1A 1A1', false));
        try {
            Address::validateGreatBritainPostalCode('SW1A 1A');
            $this->assertError((__LINE__ - 1) . ' should have thrown an exception.');
        } catch (\Exception $ex) {
            $this->assertTrue($ex instanceof \Snap\Data\Exception\InvalidPostalCodeException);
        }
        try {
            Address::validateGreatBritainPostalCode('SW1A 1A1');
            $this->assertError((__LINE__ - 1) . ' should have thrown an exception.');
        } catch (\Exception $ex) {
            $this->assertTrue($ex instanceof \Snap\Data\Exception\InvalidPostalCodeException);
        }
    }

    /**
     * @covers Snap\Data\Address::validatePostalCodeCustomRegex
     */
    public function testValidatePostalCodeCustomRegex() {
        $this->assertTrue(Address::validatePostalCodeCustomRegex('90210', '#^\d{5}([\-]?\d{4})?$#'));
        $this->assertFalse(Address::validatePostalCodeCustomRegex('9021B', '#^\d{5}([\-]?\d{4})?$#', false));
        try {
            Address::validatePostalCodeCustomRegex('902102', '#^\d{5}([\-]?\d{4})?$#');
            $this->assertError((__LINE__ - 1) . ' should have thrown an exception.');
        } catch (\Exception $ex) {
            $this->assertTrue($ex instanceof \Snap\Data\Exception\InvalidPostalCodeException);
        }
    }
     
   /**
     * @covers Snap\Data\Address::setId
     */
    public function testSetId() {
        $this->Address->setId('12345');
        $this->assertSame($this->Address->getId(), '12345');
        $this->Address->setId(54321);
        $this->assertSame($this->Address->getId(), '54321');
    }

    /**
     * @covers Snap\Data\Address::getId
     */
    public function testGetId() {
        $this->assertSame($this->Address->getId(), '');
        $this->Address->setId(99999);
        $this->assertSame($this->Address->getId(), '99999');
    }
     
   /**
     * @covers Snap\Data\Address::setDescription
     */
    public function testSetDescription() {
        $this->Address->setDescription('Work Address');
        $this->assertSame($this->Address->getDescription(), 'Work Address');
    }

    /**
     * @covers Snap\Data\Address::getDescription
     */
    public function testGetDescription() {
        $this->assertSame($this->Address->getDescription(), 'work');
        $this->Address->unsetDescription();
        $this->assertSame($this->Address->getDescription(), '');
    }

    /**
     * @covers Snap\Data\Address::setNotes
     */
    public function testSetNotes() {
        $this->Address->setNotes('This is the address where Homer Simpson works.');
        $this->assertSame($this->Address->getNotes(), 'This is the address where Homer Simpson works.');
    }

    /**
     * @covers Snap\Data\Address::getNotes
     */
    public function testGetNotes() {
        $this->assertSame($this->Address->getNotes(), 'This is my work address');
        $this->Address->unsetNotes();
        $this->assertSame($this->Address->getNotes(), '');
    }
    

}

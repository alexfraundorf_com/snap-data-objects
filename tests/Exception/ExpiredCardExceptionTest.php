<?php

namespace Snap\Data\Exception;

/**
 * Generated by PHPUnit_SkeletonGenerator on 2018-03-27 at 11:50:58.
 */
class ExpiredCardExceptionTest extends \PHPUnit_Framework_TestCase {

    /**
     * @covers Snap\Data\Exception\ExpiredCardException::__construct
     */
    public function testConstructor() {
        try {
           throw new ExpiredCardException('test', 1); 
        } catch (\Exception $ex) {
            $this->assertTrue($ex instanceof ExpiredCardException);
            $this->assertSame($ex->getMessage(), 'test');
            $this->assertSame($ex->getCode(), 1);
            $this->assertSame($ex->getPrevious(), null);
        }
    }
    
}

<?php
/**
 * Dummy class to allow properties to be unset which is not possible in the live object.
 * 
 * @package \Snap\Data
 * @author Alex Fraundorf - AlexFraundorf.com
 * @copyright (c) 2018, Alex Fraundorf and Snap Programming and Development LLC
 * @version 1.0.0 03/22/2018
 * @since 1.0.0 03/22/2018
 * @license MIT License http://www.opensource.org/licenses/mit-license.php
 */
namespace Snap\Data;


class CreditCardDummy extends CreditCard {
 
    
    public function unsetNumber() {
        $this->card_number = null;
        return $this;
    }
    
    
    public function unsetExpirationMonth() {
        $this->expiration_month = null;
        return $this;
    }
    
    
    public function setExpirationMonthWithoutValidation($month) {
        $this->expiration_month = $month;
        return $this;
    }
    
    
    public function unsetExpirationYear() {
        $this->expiration_year = null;
        return $this;
    }
    
    
    public function setExpirationYearWithoutValidation($year) {
        $this->expiration_year = $year;
        return $this;
    }
    
    
    public function unsetVerificationCode() {
        $this->verification_code = null;
        return $this;
    }
    
    
    public function unsetPostalCode() {
        $this->postal_code = null;
        return $this;
    }
    
    
}

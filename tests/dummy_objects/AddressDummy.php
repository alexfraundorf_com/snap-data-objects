<?php
/**
 * Dummy class to allow properties to be unset which is not possible in the live object.
 * 
 * @package \Snap\Data
 * @author Alex Fraundorf - AlexFraundorf.com
 * @copyright (c) 2018, Alex Fraundorf and Snap Programming and Development LLC
 * @version 1.0.0 03/22/2018
 * @since 1.0.0 03/22/2018
 * @license MIT License http://www.opensource.org/licenses/mit-license.php
 */
namespace Snap\Data;


class AddressDummy extends Address {
 
    
    public function unsetAddress1() {
        $this->address1 = null;
        return $this;
    }
      
    
    public function unsetCity() {
        $this->city = null;
        return $this;
    }
    
    
    public function unsetStateProvince() {
        $this->state_province = null;
        return $this;
    }
    
    
    public function unsetPostalCode() {
        $this->postal_code = null;
        return $this;
    }
    
    
    public function unsetDescription() {
        $this->description = null;
        return $this;
    }
    
    
    public function unsetNotes() {
        $this->notes = null;
        return $this;
    }
    
    
}

<?php

namespace Snap\Data;

/**
 * Generated by PHPUnit_SkeletonGenerator on 2018-03-15 at 14:11:52.
 */
class PhoneTest extends \PHPUnit_Framework_TestCase {

    /**
     * @var Phone
     */
    protected $Phone;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp() {
        require_once('../src/DataAbstract.php');
        require_once('../src/PhoneInterface.php');
        require_once('../src/Phone.php');
        $this->Phone = new Phone('555-123-4567');
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown() {
        
    }

    /**
     * @covers Snap\Data\Phone::__construct
     */
    public function testConstructor() {
        $Phone = new Phone('555-123-4567', 'description', 'notes');
        $this->assertSame($Phone->getPhoneNumber(), '(555) 123-4567');
        $this->assertSame($Phone->getDescription(), 'description');
        $this->assertSame($Phone->getNotes(), 'notes');
    }

    /**
     * @covers Snap\Data\Phone::setPhoneNumber
     */
    public function testSetPhoneNumber() {
        $this->Phone->setPhoneNumber('999-555-9876');
        $this->assertSame($this->Phone->getPhoneNumber(), '(999) 555-9876');
        $this->Phone->setPhoneNumber('0300 200 3300', false);
        $this->assertSame($this->Phone->getPhoneNumber(), '0300 200 3300');
    }

    /**
     * @covers Snap\Data\Phone::getPhoneNumber
     */
    public function testGetPhoneNumber() {
        $this->assertSame($this->Phone->getPhoneNumber(), '(555) 123-4567');
        $this->Phone->setPhoneNumber('999-555-9876');
        $this->assertSame($this->Phone->getPhoneNumber(), '(999) 555-9876');
    }

    /**
     * @covers Snap\Data\Phone::__toString
     */
    public function test__toString() {
        $this->assertSame((string) $this->Phone, '(555) 123-4567');
    }

    /**
     * @covers Snap\Data\Phone::validateUsPhoneNumber
     */
    public function testValidateUsPhoneNumber() {
        $this->assertTrue(Phone::validateUsPhoneNumber('15551234567'));
        $this->assertTrue(Phone::validateUsPhoneNumber('1-555-123-4567'));
        $this->assertTrue(Phone::validateUsPhoneNumber('5551234567'));
        $this->assertTrue(Phone::validateUsPhoneNumber('555-123-4567'));
        $this->assertTrue(Phone::validateUsPhoneNumber('(555) 123-4567'));
        $this->assertFalse(Phone::validateUsPhoneNumber('(555) 123-4567 x1234', false));
        $this->assertFalse(Phone::validateUsPhoneNumber('555-123-456', false));
        $this->assertFalse(Phone::validateUsPhoneNumber('123-4567', false));
        try {
            Phone::validateUsPhoneNumber('555-123-456');
            $this->assertError((__LINE__ - 1) . ' should have thrown an exception.');
        } catch (\Exception $ex) {
            $this->assertTrue($ex instanceof \Snap\Data\Exception\InvalidPhoneException);
        }
        try {
            Phone::validateUsPhoneNumber('123-4567');
            $this->assertError((__LINE__ - 1) . ' should have thrown an exception.');
        } catch (\Exception $ex) {
            $this->assertTrue($ex instanceof \Snap\Data\Exception\InvalidPhoneException);
        }        
    }

    /**
     * @covers Snap\Data\Phone::formatPhoneNumber
     */
    public function testFormatPhoneNumber() {
        $this->assertSame(Phone::formatPhoneNumber('1-555-123-4567'), '1 (555) 123-4567');
        $this->assertSame(Phone::formatPhoneNumber('15551234567'), '1 (555) 123-4567');
        $this->assertSame(Phone::formatPhoneNumber('555-123-4567 ext. 1234'), '(555) 123-4567 x1234');
        $this->assertSame(Phone::formatPhoneNumber('55512345675555'), '(555) 123-4567 x5555');
        $this->assertSame(Phone::formatPhoneNumber('555-123-4567'), '(555) 123-4567');
        $this->assertSame(Phone::formatPhoneNumber('5551234567'), '(555) 123-4567');
        $this->assertSame(Phone::formatPhoneNumber('(555)1234567'), '(555) 123-4567');
        $this->assertSame(Phone::formatPhoneNumber('555 123 4567'), '(555) 123-4567');
        $this->assertSame(Phone::formatPhoneNumber('555.123.4567'), '(555) 123-4567');
    }

    
    /**
     * @covers Snap\Data\Phone::setId
     */
    public function testSetId() {
        $this->Phone->setId('12345');
        $this->assertSame($this->Phone->getId(), '12345');
    }

    /**
     * @covers Snap\Data\Phone::getId
     */
    public function testGetId() {
        $this->Phone->setId(12345);
        $this->assertSame($this->Phone->getId(), '12345');
    }

    /**
     * @covers Snap\Data\Phone::setDescription
     */
    public function testSetDescription() {
        $this->Phone->setDescription('work phone');
        $this->assertSame($this->Phone->getDescription(), 'work phone');
    }

    /**
     * @covers Snap\Data\Phone::getDescription
     */
    public function testGetDescription() {
        $this->Phone->setDescription('work phone');
        $this->assertSame($this->Phone->getDescription(), 'work phone');
    }

    /**
     * @covers Snap\Data\Phone::setNotes
     */
    public function testSetNotes() {
        $this->Phone->setNotes('This is a boring note.');
        $this->assertSame($this->Phone->getNotes(), 'This is a boring note.');
    }

    /**
     * @covers Snap\Data\Phone::getNotes
     */
    public function testGetNotes() {
        $this->Phone->setNotes('This is a boring note.');
        $this->assertSame($this->Phone->getNotes(), 'This is a boring note.');
    }    
    
}
